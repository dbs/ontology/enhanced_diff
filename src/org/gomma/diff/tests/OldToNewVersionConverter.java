package org.gomma.diff.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.gomma.diff.utils.DataBaseHandler;
import org.gomma.diff.utils.Utils;

public class OldToNewVersionConverter {

	public static String TABLE_NAME = "migration_old_version";
	public static String TABLE_SCHEMA = "CREATE TABLE migration_old_version ("+
										  "values_md5 varchar(50) NOT NULL,"+
										  "element_type varchar(50) NOT NULL,"+
										  "value1 varchar(2000) default NULL,"+
										  "value2 varchar(2000) default NULL,"+
										  "value3 varchar(2000) default NULL,"+
										  "PRIMARY KEY  (values_md5),"+
										  "KEY value1 (value1(1000)),"+
										  "KEY value2 (value2(1000)),"+
										  "KEY value3 (value3(1000)))";
	public static String EDIT_SCRIPT = "H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/GO-BP/migration/low_level_actions.txt";
	
	public HashMap<String, List<String[]>> changeActions = new HashMap<String, List<String[]>>();
	
	public static void main(String[] args) {
		OldToNewVersionConverter test = new OldToNewVersionConverter();
		test.readChanges(EDIT_SCRIPT);
		test.applyChangesToVersion();
		test.inverseChanges();
		test.applyChangesToVersion();
		//test.inverseChanges();
		//test.applyChangesToVersion();
	}
	
	public void readChanges(String fileLocation) {
		try {
			RandomAccessFile file = new RandomAccessFile(fileLocation,"r");
			String line;
			while ((line=file.readLine())!=null) {
				if (line.trim().length()>0) {
					String[] lineSplit = line.split("\t");
					if (lineSplit[1].equals("addObj")||lineSplit[1].equals("delObj")) {
						List<String[]> currentActions = changeActions.get(lineSplit[1]);
						if (currentActions==null) {
							currentActions = new Vector<String[]>();
						}
						currentActions.add(new String[]{lineSplit[2].trim()});
						changeActions.put(lineSplit[1], currentActions);
					} else if (lineSplit[1].equals("mapObj")) {
						List<String[]> currentActions = changeActions.get(lineSplit[1]);
						if (currentActions==null) {
							currentActions = new Vector<String[]>();
						}
						currentActions.add(new String[]{lineSplit[2].trim(),lineSplit[3].trim()});
						changeActions.put(lineSplit[1], currentActions);
					} else if (lineSplit[1].equals("addAtt")||lineSplit[1].equals("addAss")||lineSplit[1].equals("delAtt")||lineSplit[1].equals("delAss")) {
						List<String[]> currentActions = changeActions.get(lineSplit[1]);
						if (currentActions==null) {
							currentActions = new Vector<String[]>();
						}
						String[] valueSplit = lineSplit[4].split("#");
						for (int i=0;i<valueSplit.length;i++) {
							currentActions.add(new String[]{lineSplit[2].trim(),lineSplit[3].trim(),valueSplit[i].trim()});
						}
						changeActions.put(lineSplit[1], currentActions);
					} else if (lineSplit[1].equals("mapAtt")||lineSplit[1].equals("mapAss")) {
						List<String[]> currentActions = changeActions.get(lineSplit[1]);
						if (currentActions==null) {
							currentActions = new Vector<String[]>();
						}
						currentActions.add(new String[]{lineSplit[2].trim(),lineSplit[3].trim(),lineSplit[4].trim(),lineSplit[5].trim(),lineSplit[6].trim(),lineSplit[7].trim()});
						changeActions.put(lineSplit[1], currentActions);
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void inverseChanges() {
		HashMap<String, List<String[]>> inverseChanges = new HashMap<String, List<String[]>>();
		for (String action : changeActions.keySet()) {
			if (action.equals("addAss")) {
				inverseChanges.put("delAss", changeActions.get(action));
			} else if (action.equals("delAss")) {
				inverseChanges.put("addAss", changeActions.get(action));
			} else if (action.equals("addAtt")) {
				inverseChanges.put("delAtt", changeActions.get(action));
			} else if (action.equals("delAtt")) {
				inverseChanges.put("addAtt", changeActions.get(action));
			} else if (action.equals("addObj")) {
				inverseChanges.put("delObj", changeActions.get(action));
			} else if (action.equals("delObj")) {
				inverseChanges.put("addObj", changeActions.get(action));
			} else if (action.equals("mapObj")) {
				List<String[]> newActions = new Vector<String[]>();
				List<String[]> actions = changeActions.get(action);
				for (int i=0;i<actions.size();i++) {
					String[] actionValues = actions.get(i);
					String[] newActionValues = new String[]{actionValues[1],actionValues[0]};
					newActions.add(newActionValues);
				}
				inverseChanges.put("mapObj", newActions);
			} else if (action.equals("mapAss")) {
				List<String[]> newActions = new Vector<String[]>();
				List<String[]> actions = changeActions.get(action);
				for (int i=0;i<actions.size();i++) {
					String[] actionValues = actions.get(i);
					String[] newActionValues = new String[]{actionValues[3],actionValues[4],actionValues[5],actionValues[0],actionValues[1],actionValues[2]};
					newActions.add(newActionValues);
				}
				inverseChanges.put("mapAss", newActions);
			} else if (action.equals("mapAtt")) {
				List<String[]> newActions = new Vector<String[]>();
				List<String[]> actions = changeActions.get(action);
				for (int i=0;i<actions.size();i++) {
					String[] actionValues = actions.get(i);
					String[] newActionValues = new String[]{actionValues[3],actionValues[4],actionValues[5],actionValues[0],actionValues[1],actionValues[2]};
					newActions.add(newActionValues);
				}
				inverseChanges.put("mapAtt", newActions);
			}
		}
		this.changeActions = inverseChanges;
	}
	
	public void applyChangesToVersion() {
		applyChanges("delAss");
		applyChanges("delAtt");
		applyChanges("delObj");
		applyChanges("addObj");
		applyChanges("mapObj");
		applyChanges("addAtt");
		applyChanges("mapAtt");
		applyChanges("addAss");
		applyChanges("mapAss");
	}
	
	private void applyChanges(String actionType) {
		List<String[]> changes = changeActions.get(actionType);
		if (changes!=null&&changes.size()>0) {
			if (actionType.equalsIgnoreCase("delAss")) {
				applyDelAss(changes);
			} else if (actionType.equalsIgnoreCase("delAtt")) {
				applyDelAtt(changes);
			} else if (actionType.equalsIgnoreCase("delObj")) {
				applyDelObj(changes);
			} else if (actionType.equalsIgnoreCase("addObj")) {
				applyAddObj(changes);
			} else if (actionType.equalsIgnoreCase("mapObj")) {
				applyMapObj(changes);
			} else if (actionType.equalsIgnoreCase("addAtt")) {
				applyAddAtt(changes);
			} else if (actionType.equalsIgnoreCase("mapAtt")) {
				applyMapAtt(changes);
			} else if (actionType.equalsIgnoreCase("addAss")) {
				applyAddAss(changes);
			} else if (actionType.equalsIgnoreCase("mapAss")) {
				applyMapAss(changes);
			}
		}
	}
	
	private void applyDelAss(List<String[]> changes) {
		try {
			String query = "DELETE FROM "+TABLE_NAME+" WHERE element_type = 'association' AND value1=? AND value2=? AND value3=?";
			PreparedStatement stmt = DataBaseHandler.getInstance().prepareStatement(query);
			for (String[] change : changes) {
				stmt.setString(1, change[0]);
				stmt.setString(2, change[1]);
				stmt.setString(3, change[2]);
				stmt.addBatch();
			}
			stmt.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void applyDelAtt(List<String[]> changes) {
		try {
			String query = "DELETE FROM "+TABLE_NAME+" WHERE element_type = 'attribute' AND value1=? AND value2=? AND value3=?";
			PreparedStatement stmt = DataBaseHandler.getInstance().prepareStatement(query);
			for (String[] change : changes) {
				stmt.setString(1, change[0]);
				stmt.setString(2, change[1]);
				stmt.setString(3, change[2]);
				stmt.addBatch();
			}
			stmt.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void applyDelObj(List<String[]> changes) {
		try {
			String query = "DELETE FROM "+TABLE_NAME+" WHERE element_type = 'object' AND value1=?";
			PreparedStatement stmt = DataBaseHandler.getInstance().prepareStatement(query);
			for (String[] change : changes) {
				stmt.setString(1, change[0]);
				stmt.addBatch();
			}
			stmt.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void applyAddObj(List<String[]> changes) {
		try {
			String query = "INSERT INTO "+TABLE_NAME+" (values_md5,element_type,value1) VALUES (?,'object',?)";
			PreparedStatement stmt = DataBaseHandler.getInstance().prepareStatement(query);
			for (String[] change : changes) {
				stmt.setString(1, Utils.MD5(new String[]{"object",change[0]}));
				stmt.setString(2, change[0]);
				stmt.addBatch();
			}
			stmt.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void applyMapObj(List<String[]> changes) {
		try {
			String query1 = "DELETE FROM "+TABLE_NAME+" WHERE element_type = 'object' AND value1=?";
			String query2 = "REPLACE "+TABLE_NAME+" (values_md5,element_type,value1) VALUES (?,'object',?)";
			PreparedStatement stmt1 = DataBaseHandler.getInstance().prepareStatement(query1);
			PreparedStatement stmt2 = DataBaseHandler.getInstance().prepareStatement(query2);
			for (String[] change : changes) {
				stmt1.setString(1, change[0]);
				stmt1.addBatch();
				stmt2.setString(1, Utils.MD5(new String[]{"object",change[1]}));
				stmt2.setString(2, change[1]);
				stmt2.addBatch();
			}
			stmt1.executeBatch();
			stmt2.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void applyAddAtt(List<String[]> changes) {
		try {
			String query = "REPLACE "+TABLE_NAME+" (values_md5,element_type,value1,value2,value3) VALUES (?,'attribute',?,?,?)";
			PreparedStatement stmt = DataBaseHandler.getInstance().prepareStatement(query);
			for (String[] change : changes) {
				stmt.setString(1, Utils.MD5(new String[]{"attribute",change[0],change[1],change[2]}));
				stmt.setString(2, change[0]);
				stmt.setString(3, change[1]);
				stmt.setString(4, change[2]);
				stmt.addBatch();
			}
			stmt.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void applyMapAtt(List<String[]> changes) {
		try {
			String query1 = "DELETE FROM "+TABLE_NAME+" WHERE element_type = 'attribute' AND value1=? AND value2=? AND value3=?";
			String query2 = "REPLACE "+TABLE_NAME+" (values_md5,element_type,value1,value2,value3) VALUES (?,'attribute',?,?,?)";
			PreparedStatement stmt1 = DataBaseHandler.getInstance().prepareStatement(query1);
			PreparedStatement stmt2 = DataBaseHandler.getInstance().prepareStatement(query2);
			for (String[] change : changes) {
				stmt1.setString(1, change[0]);
				stmt1.setString(2, change[1]);
				stmt1.setString(3, change[2]);
				stmt1.addBatch();
				stmt2.setString(1, Utils.MD5(new String[]{"attribute",change[3],change[4],change[5]}));
				stmt2.setString(2, change[3]);
				stmt2.setString(3, change[4]);
				stmt2.setString(4, change[5]);
				stmt2.addBatch();
			}
			stmt1.executeBatch();
			stmt2.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void applyAddAss(List<String[]> changes) {
		try {
			String query = "REPLACE "+TABLE_NAME+" (values_md5,element_type,value1,value2,value3) VALUES (?,'association',?,?,?)";
			PreparedStatement stmt = DataBaseHandler.getInstance().prepareStatement(query);
			for (String[] change : changes) {
				stmt.setString(1, Utils.MD5(new String[]{"association",change[0],change[1],change[2]}));
				stmt.setString(2, change[0]);
				stmt.setString(3, change[1]);
				stmt.setString(4, change[2]);
				stmt.addBatch();
			}
			stmt.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void applyMapAss(List<String[]> changes) {
		try {
			String query1 = "DELETE FROM "+TABLE_NAME+" WHERE element_type = 'association' AND value1=? AND value2=? AND value3=?";
			String query2 = "REPLACE "+TABLE_NAME+" (values_md5,element_type,value1,value2,value3) VALUES (?,'association',?,?,?)";
			PreparedStatement stmt1 = DataBaseHandler.getInstance().prepareStatement(query1);
			PreparedStatement stmt2 = DataBaseHandler.getInstance().prepareStatement(query2);
			for (String[] change : changes) {
				stmt1.setString(1, change[0]);
				stmt1.setString(2, change[1]);
				stmt1.setString(3, change[2]);
				stmt1.addBatch();
				stmt2.setString(1, Utils.MD5(new String[]{"association",change[3],change[4],change[5]}));
				stmt2.setString(2, change[3]);
				stmt2.setString(3, change[4]);
				stmt2.setString(4, change[5]);
				stmt2.addBatch();
			}
			stmt1.executeBatch();
			stmt2.executeBatch();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
