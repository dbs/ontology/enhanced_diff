package org.gomma.diff.tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.gomma.Gomma;
import org.gomma.GommaImpl;
import org.gomma.match.MatchProperties;
import org.gomma.match.metrics.StringSimilarityFunctions;
import org.gomma.model.attribute.Attribute;
import org.gomma.model.attribute.AttributeSet;
import org.gomma.model.attribute.AttributeValueVersion;
import org.gomma.model.mapping.Mapping;
import org.gomma.model.mapping.ObjCorrespondence;
import org.gomma.model.mapping.ObjCorrespondenceSet;
import org.gomma.model.source.Obj;
import org.gomma.model.source.ObjRelationship;
import org.gomma.model.source.ObjRelationshipSet;
import org.gomma.model.source.ObjSet;
import org.gomma.model.source.Source;
import org.gomma.model.source.SourceSet;
import org.gomma.model.source.SourceVersion;
import org.gomma.model.source.SourceVersionStructure;
import org.gomma.util.math.AggregationFunction;

public class GommaMatchTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		//doMatch("Category","DMozStructure","2008-09","2009-09","DMozStructure-Evolution-Mapping","DMozStructure-Evolution-Mapping 2008_09-2009-09");
		doMatch("MidEurope","DMozStructure","2008-09","2009-09","DMozStructure-MidEurope-Evolution-Mapping","DMozStructure-MidEurope-Evolution-Mapping 2008_09-2009-09");
	}
	
	
	public static void doMatch(String objType,String pdsName,String oldVersionName, String newVersionName,String perfectMappingName,String perfectMapppingVersionName) throws Exception {
		Gomma gi = new GommaImpl();
		gi.initialize("E:/Daten_Hartung/workspace3.4/Ontology Regions/gomma-dmoz.ini");
		
		SourceSet sources = gi.getSourceManager().getSourceSet(objType, pdsName);
		
		Source source = null;
		for (int key : sources.getIDSet()) {
			source = sources.getElement(key);
		}
		
		SourceVersion oldVersion = gi.getSourceManager().getSourceVersionSet(source, oldVersionName).getEarliestSourceVersion();
		SourceVersion newVersion = gi.getSourceManager().getSourceVersionSet(source, newVersionName).getEarliestSourceVersion();
		
		SourceVersionStructure oldVersionStruc = gi.getSourceManager().getSourceStructure(oldVersion);
		SourceVersionStructure newVersionStruc = gi.getSourceManager().getSourceStructure(newVersion);
		
		
		MatchProperties props = new MatchProperties();
		props.addDomainAttributeName("name");
		props.addRangeAttributeName("name");
		
		ObjCorrespondenceSet exactMatch = gi.getMatchManager().match(oldVersionStruc, newVersionStruc, StringSimilarityFunctions.EXACT, AggregationFunction.AVERAGE, 1.0f, props);
		
		ObjSet domainObjs = gi.getSourceManager().getObjectSet(oldVersion, exactMatch.getDomainIDObjectSet());
		ObjSet rangeObjs = gi.getSourceManager().getObjectSet(newVersion, exactMatch.getRangeIDObjectSet());
		
		ObjSet deletedObjs = gi.getSourceManager().diff(oldVersionStruc.getAllObjects(),domainObjs);
		ObjSet addedObjs = gi.getSourceManager().diff(newVersionStruc.getAllObjects(),rangeObjs);
		
		
		props = new MatchProperties();
		props.addDomainAttributeName("name");
		props.addRangeAttributeName("name");
		ObjCorrespondenceSet renameMatch1 = gi.getMatchManager().match(gi.getSourceManager().getSourceStructurePortion(oldVersion, deletedObjs), gi.getSourceManager().getSourceStructurePortion(newVersion, addedObjs), StringSimilarityFunctions.LEVENSHTEIN, AggregationFunction.AVERAGE, 0.85f, props);

		props = new MatchProperties();
		props.addDomainAttributeName("synonym");
		props.addRangeAttributeName("synonym");
		ObjCorrespondenceSet renameMatch2 = gi.getMatchManager().match(gi.getSourceManager().getSourceStructurePortion(oldVersion, deletedObjs), gi.getSourceManager().getSourceStructurePortion(newVersion, addedObjs), StringSimilarityFunctions.LEVENSHTEIN, AggregationFunction.AVERAGE, 0.75f, props);
		
		ObjCorrespondenceSet renameMatch = gi.getMappingOperationManager().intersect(renameMatch1, renameMatch2);
		
		deletedObjs = gi.getSourceManager().diff(deletedObjs,gi.getSourceManager().getObjectSet(oldVersion, renameMatch.getDomainIDObjectSet()));
		addedObjs = gi.getSourceManager().diff(addedObjs,gi.getSourceManager().getObjectSet(newVersion, renameMatch.getRangeIDObjectSet()));
		
		//ObjCorrespondenceSet mergeMatch = gi.getMatchManager().match(gi.getSourceManager().getSourceStructurePortion(oldVersion, deletedObjs), newVersionStruc, MatcherType.NAME, StringSimilarityFunction.PREFIX_OVERLAP, AggregationFunction.AVERAGE, 0.4f, props);
		//ObjCorrespondenceSet splitMatch = gi.getMatchManager().match(oldVersionStruc, gi.getSourceManager().getSourceStructurePortion(newVersion, addedObjs), MatcherType.NAME, StringSimilarityFunction.PREFIX_OVERLAP, AggregationFunction.AVERAGE, 0.4f, props);
		//ObjCorrespondenceSet mergeMatch = gi.getMappingOperationManager().intersect(mergeMatch1, mergeMatch2);
		//mergeMatch = getBestOneToOneMapping(mergeMatch,true);
		//splitMatch = getBestOneToOneMapping(splitMatch,false);
		
		ObjCorrespondenceSet finalResult = gi.getMappingOperationManager().union(exactMatch, renameMatch);
		System.out.println(finalResult.size());
		
		Mapping perfectMapppings = gi.getMappingManager().getMappingSet(source,source).getMapping(perfectMappingName);
		ObjCorrespondenceSet perfectResult = gi.getMappingManager().getObjectCorrespondenceSet(gi.getMappingManager().getMappingVersionSet(perfectMapppings).getMappingVersion(perfectMapppingVersionName));
		
		ObjCorrespondenceSet tp = gi.getMappingOperationManager().intersect(finalResult, perfectResult);
		ObjCorrespondenceSet fp = gi.getMappingOperationManager().diff(finalResult, perfectResult);
		ObjCorrespondenceSet fn = gi.getMappingOperationManager().diff(perfectResult, finalResult);
		
		float recall = ((float)tp.size())/((float)(tp.size()+fn.size()));
		float precision = ((float)tp.size())/((float)(tp.size()+fp.size()));
		System.out.println("Recall: "+recall+"   Precision: "+precision);
		
		System.out.println("False positives: ");
		printMatchResultsWithNames(fp, oldVersionStruc, newVersionStruc);
		
		System.out.println("False negatives: ");
		printMatchResultsWithNames(fn, oldVersionStruc, newVersionStruc);
		
		ObjRelationshipSet addedRels = diff(newVersionStruc.getAllRelationships(),oldVersionStruc.getAllRelationships());
		ObjRelationshipSet deletedRels = diff(oldVersionStruc.getAllRelationships(),newVersionStruc.getAllRelationships());
		ObjRelationshipSet matchRels = intersect(oldVersionStruc.getAllRelationships(),newVersionStruc.getAllRelationships());
		
		
		List<String[]> mapObj = matchAddDelObjectToMappingWriter(finalResult, addedObjs, deletedObjs);
		List<String[]> mapAss = matchAddDelAssociationToMappingWriter(oldVersionStruc, newVersionStruc, matchRels, addedRels, deletedRels);
		
		//writeToFile(mapObj, "H:/Papers-Talks-Infos/Own/Ontology Version Diff/DMoz-Example-MidEurope/Obj_Mapping.txt");
		writeToFile(mapAss, "H:/Papers-Talks-Infos/Own/Ontology Version Diff/DMoz-Example-MidEurope/Ass_Mapping.txt");
	
	}
	
	private static void printMatchResultsWithNames(ObjCorrespondenceSet match,SourceVersionStructure domain,SourceVersionStructure range) {
		for (ObjCorrespondence c : match.getCollection()) {
			// get Name f�r ein Object (name zur accession)
			Obj domainObj = domain.getObject(c.getDomainObjID());
			Obj rangeObj = range.getObject(c.getRangeObjID());
			String domObjName = getConceptNamesForCorr(domainObj);
			String ranObjName = getConceptNamesForCorr(rangeObj);
			
			 System.out.println( c.getDomainAccessionNumber() + "    " +
			 domObjName + "    " + c.getRangeAccessionNumber()+ "    " +
			 ranObjName + "    " + c.getConfidence());
			 
			/*Logger.getSingleton().writeToLog(
					c.getDomainAccessionNumber() + "    " + domObjName + "    "
							+ c.getRangeAccessionNumber() + "    " + ranObjName
							+ "    " + c.getConfidence() + "    fp");*/
		} 
	}
	
	private static ObjCorrespondenceSet getBestOneToOneMapping(ObjCorrespondenceSet match, boolean domain) {
		ObjCorrespondenceSet result = new ObjCorrespondenceSet();
		HashMap<String, ObjCorrespondence> filteredResult = new HashMap<String, ObjCorrespondence>();
		if (domain) {
			for (ObjCorrespondence corr : match.getCollection()) {
				ObjCorrespondence tmp = filteredResult.get(corr.getDomainAccessionNumber());
				if (tmp!=null) {
					if (corr.getConfidence()>tmp.getConfidence()) {
						filteredResult.put(corr.getDomainAccessionNumber(), corr);
					}
				} else {
					filteredResult.put(corr.getDomainAccessionNumber(), corr);
				}
			}
		} else {
			for (ObjCorrespondence corr : match.getCollection()) {
				ObjCorrespondence tmp = filteredResult.get(corr.getRangeAccessionNumber());
				if (tmp!=null) {
					if (corr.getConfidence()>tmp.getConfidence()) {
						filteredResult.put(corr.getRangeAccessionNumber(), corr);
					}
				} else {
					filteredResult.put(corr.getRangeAccessionNumber(), corr);
				}
			}
		}
		for (String key : filteredResult.keySet()) {
			//result.addCorrespondence(filteredResult.get(key));
		}
		return result;
 	}
	
	public static String getConceptNamesForCorr(Obj thisObj) {
		AttributeSet atts = thisObj.getAttributeValues().getAttributeSet();
		String name = "";

		for (Attribute att : atts.getAttributeSet("name").getCollection()) {
			for (AttributeValueVersion avv : thisObj.getAttributeValues(att)
					.getCollection()) {
				name = avv.toString();
			}
		}
		return name;
	}

	public static ObjRelationshipSet diff(ObjRelationshipSet op1,ObjRelationshipSet op2) {
		ObjRelationshipSet result = new ObjRelationshipSet();
		for (ObjRelationship rel : op1.getCollection()) {
			if (!op2.containsRelationship(rel.getID())) {
				result.addObjectRelationship(rel);
			}
		}
		return result;
	}
	
	public static ObjRelationshipSet intersect(ObjRelationshipSet op1,ObjRelationshipSet op2) {
		ObjRelationshipSet result = new ObjRelationshipSet();
		for (ObjRelationship rel : op1.getCollection()) {
			if (op2.containsRelationship(rel.getID())) {
				result.addObjectRelationship(rel);
			}
		}
		return result;
	}
	
	public static List<String[]> matchAddDelObjectToMappingWriter(ObjCorrespondenceSet match,ObjSet added,ObjSet deleted) {
		//Umschreiben auf Repr�sentation f�r Rule_Engine
		List<String[]> result = new Vector<String[]>();
		for (ObjCorrespondence c : match.getCollection()) {
			result.add(new String[]{"mapObjObj",c.getDomainAccessionNumber(),c.getRangeAccessionNumber()});
		}
		for (Obj o : added.getCollection()) {
			result.add(new String[]{"mapObjObj"," ",o.getAccessionNumber()});
		}
		for (Obj o : deleted.getCollection()) {
			result.add(new String[]{"mapObjObj",o.getAccessionNumber()," "});
		}
		return result;
	}
	
	public static List<String[]> matchAddDelAssociationToMappingWriter(SourceVersionStructure oldVersion,SourceVersionStructure newVersion,ObjRelationshipSet match,ObjRelationshipSet added,ObjRelationshipSet deleted) {
		List<String[]> result = new Vector<String[]>();
		for (ObjRelationship r : match.getCollection()) {
			String fromAccession = oldVersion.getObject(r.getToID()).getAccessionNumber();
			String toAccession = oldVersion.getObject(r.getFromID()).getAccessionNumber();
			String type = r.getRelationshipType();
			result.add(new String[]{"mapAssAss",fromAccession,type,toAccession,fromAccession,type,toAccession});
		}
		for (ObjRelationship r : added.getCollection()) {
			String fromAccession = newVersion.getObject(r.getToID()).getAccessionNumber();
			String toAccession = newVersion.getObject(r.getFromID()).getAccessionNumber();
			String type = r.getRelationshipType();
			result.add(new String[]{"mapAssAss"," "," "," ",fromAccession,type,toAccession});
		}
		for (ObjRelationship r : deleted.getCollection()) {
			String fromAccession = oldVersion.getObject(r.getToID()).getAccessionNumber();
			String toAccession = oldVersion.getObject(r.getFromID()).getAccessionNumber();
			String type = r.getRelationshipType();
			result.add(new String[]{"mapAssAss",fromAccession,type,toAccession," "," "," "});
		}
		return result;
	}
	
	public static void writeToFile(List<String[]> data,String fileLocation) {
		try {
			RandomAccessFile file = new RandomAccessFile(fileLocation,"rw");
			for (int i=0;i<data.size();i++) {
				String[] entry = data.get(i);
				String line = entry[0];
				for (int j=1;j<entry.length;j++) {
					line += "\t"+entry[j];
				}
				file.writeBytes(line+"\n");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
