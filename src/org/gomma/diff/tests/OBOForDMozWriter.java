package org.gomma.diff.tests;

import java.io.RandomAccessFile;


import org.gomma.diff.utils.Utils;

public class OBOForDMozWriter {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		RandomAccessFile file = new RandomAccessFile("E:/Daten_Hartung/DMoz-Evaluierung/OnlySoccer/categories_2010_01_soccer.txt","r");
		RandomAccessFile writeFile = new RandomAccessFile("E:/Daten_Hartung/DMoz-Evaluierung/dmoz_soccer_2010_01.obo","rw");
		String line;
		while ((line=file.readLine())!=null) {
			String[] lineSplit = line.split("/");
			String simpleName = lineSplit[lineSplit.length-1];
			String fullName = line;
			String accession = Utils.MD5(fullName);
			String superCategory = null;
			if (lineSplit.length>1) {
				superCategory = "";
				for (int i=0;i<lineSplit.length-1;i++) {
					superCategory += lineSplit[i]+"/";
				}
				superCategory = superCategory.substring(0, superCategory.length()-1);
				superCategory = Utils.MD5(superCategory);
			}
			String term= "[Term]\nid: "+accession+"\nname: "+fullName+"\nsynonym: \""+simpleName+"\" [EXACT]\n";
			if (superCategory!=null) {
				term += "is_a: "+superCategory+"\n";
			}
			term += "\n";
			writeFile.writeBytes(term);
		}
	}

}
