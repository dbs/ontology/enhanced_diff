package org.gomma.diff;

import java.util.List;
import java.util.Properties;
import java.util.Vector;

import org.gomma.diff.match.MatchConfig;
import org.gomma.diff.match.MatchExecutor;
import org.gomma.diff.match.MatchGlobals;
import org.gomma.diff.utils.Utils;


public class DiffTester {

	/**
	 * @param args
	 */
	public static void main(String[] args)  {
		long start = System.currentTimeMillis();
		//doGOCCExample();
		//doGOMFExample();
		//doGOBPExample();
		//doNCIExample();
		//doEnsemblExample();
		//doMD5Example();
		//doMatchExample();
		//doDMozSoccerExample();
		//doDMozMidEuropeExample();
		//doGOCC2Example();
		//doGOMF2Example();
		doGOBP2Example();
		System.out.println("-------\nOverall elapsed time: "+(System.currentTimeMillis()-start)+" ms");
		
	}
	
	public static void doMatchExample() {
		//Match Example for GO CC between 2007-05 and 2007-06
		DiffExecutor.getSingleton().integrateVersionData("H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/integration_GO_CC.sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("H:/Papers-Talks-Infos/Own/Ontology Version Diff/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/Rule_GO.xml");
		
		List<MatchConfig> l = new Vector<MatchConfig>();
		Properties props;
		MatchConfig config;
		
		config = new MatchConfig(MatchGlobals.OBJ_OBJ_MATCH,MatchGlobals.FULL_EXACT_MATCH,new Properties());
		l.add(config);
		
		config = new MatchConfig(MatchGlobals.OBJ_ATT_MATCH,MatchGlobals.VALUE_EXACT_MATCH,new Properties());
		l.add(config);
		
		props = new Properties();
		props.put("att_name", "synonym");
		config = new MatchConfig(MatchGlobals.ATT_ATT_MATCH,MatchGlobals.FULL_EXACT_MATCH,props);
		l.add(config);
		
		props = new Properties();
		props.put("att_name", "name");
		config = new MatchConfig(MatchGlobals.ATT_ATT_MATCH,MatchGlobals.TYPE_EXACT_MATCH,props);
		l.add(config);
		
		props = new Properties();
		props.put("att_name", "isObsolete");
		config = new MatchConfig(MatchGlobals.ATT_ATT_MATCH,MatchGlobals.TYPE_EXACT_MATCH,props);
		l.add(config);
		
		props = new Properties();
		props.put("att_name", "definition");
		config = new MatchConfig(MatchGlobals.ATT_ATT_MATCH,MatchGlobals.TYPE_EXACT_MATCH,props);
		l.add(config);
		
		config = new MatchConfig(MatchGlobals.ASS_ASS_MATCH,MatchGlobals.FULL_EXACT_MATCH,new Properties());
		l.add(config);
		
		new MatchExecutor(l).executeBasicDiff();
	}
	
	public static void doGOCCExample() {
		//GO Example: GO CC between 2007-05 and 2007-06
		DiffExecutor.getSingleton().integrateVersionData("H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/integration_GO_CC.sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("H:/Papers-Talks-Infos/Own/Ontology Version Diff/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/Rule_GO.xml");
		DiffExecutor.getSingleton().computeBasicChanges();
		DiffExecutor.getSingleton().applyRules();
		DiffExecutor.getSingleton().mergeResultActions();
		DiffExecutor.getSingleton().reduceResultActions();
	}
	
	public static void doGOCC2Example() {
		//GO Example: GO CC between 2007-05 and 2007-06
		//DiffExecutor.getSingleton().setupRepository();
		DiffExecutor.getSingleton().integrateVersionData("H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/GO-CC/integration.sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/GO-CC/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/GO-CC/Rule_GO.xml");
		//DiffExecutor.getSingleton().computeBasicChanges();
		DiffExecutor.getSingleton().applyRules();
		DiffExecutor.getSingleton().mergeResultActions();
		//DiffExecutor.getSingleton().reduceResultActions();
		DiffExecutor.getSingleton().retrieveAndStoreHighLevelActions();
	}
	
	public static void doGOMF2Example() {
		//GO Example: GO MF between 2007-05 and 2007-06
		//DiffExecutor.getSingleton().setupRepository();
		DiffExecutor.getSingleton().integrateVersionData("H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/GO-MF/integration.sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/GO-MF/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/GO-MF/Rule_GO.xml");
		//DiffExecutor.getSingleton().computeBasicChanges();
		DiffExecutor.getSingleton().applyRules();
		DiffExecutor.getSingleton().mergeResultActions();
		//DiffExecutor.getSingleton().reduceResultActions();
		DiffExecutor.getSingleton().retrieveAndStoreHighLevelActions();
	}
	
	public static void doGOMFExample() {
		//GO Example: GO MF between 2007-05 and 2007-06
		DiffExecutor.getSingleton().integrateVersionData("H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/integration_GO_MF.sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("H:/Papers-Talks-Infos/Own/Ontology Version Diff/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/Rule_GO.xml");
		DiffExecutor.getSingleton().computeBasicChanges();
		DiffExecutor.getSingleton().applyRules();
		DiffExecutor.getSingleton().mergeResultActions();
		DiffExecutor.getSingleton().reduceResultActions();
	}
	
	public static void doGOBP2Example() {
		//GO Example: GO BP between 2007-05 and 2007-06
		//DiffExecutor.getSingleton().setupRepository();
		DiffExecutor.getSingleton().integrateVersionData("E:/workspace/enhancedDiff/GeneOntology-Evaluation/GO-BP/integration.sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("E:/workspace/enhancedDiff/GeneOntology-Evaluation/GO-BP/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("E:/workspace/enhancedDiff/GeneOntology-Evaluation/GO-BP/Rule_GO.xml");
		//DiffExecutor.getSingleton().computeBasicChanges();
		DiffExecutor.getSingleton().applyRules();
		DiffExecutor.getSingleton().mergeResultActions();
		//DiffExecutor.getSingleton().reduceResultActions();
		DiffExecutor.getSingleton().retrieveAndStoreHighLevelActions();
	}
	
	public static void doGOBPExample() {
		//GO Example: GO BP between 2007-05 and 2007-06
		DiffExecutor.getSingleton().integrateVersionData("H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/integration_GO_BP.sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("H:/Papers-Talks-Infos/Own/Ontology Version Diff/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("H:/Papers-Talks-Infos/Own/Ontology Version Diff/GO-Example/Rule_GO.xml");
		DiffExecutor.getSingleton().computeBasicChanges();
		//DiffExecutor.getSingleton().applyRules();
		//DiffExecutor.getSingleton().mergeResultActions();
		//DiffExecutor.getSingleton().reduceResultActions();
	}
	public static void doNCIExample() {
		//NCI Example: NCI between 2007-05 and 2007-06
		DiffExecutor.getSingleton().integrateVersionData("H:/Papers-Talks-Infos/Own/Ontology Version Diff/NCI-Example/integration.sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("H:/Papers-Talks-Infos/Own/Ontology Version Diff/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("H:/Papers-Talks-Infos/Own/Ontology Version Diff/NCI-Example/Rule_NCI.xml");
		DiffExecutor.getSingleton().computeBasicChanges(false);
		DiffExecutor.getSingleton().applyRules();
		DiffExecutor.getSingleton().mergeResultActions();
		DiffExecutor.getSingleton().reduceResultActions();
	}
	public static void doEnsemblExample() {
		//Ensembl Example: Ensembl proteins between version 49 and 50
		/*DiffExecutor.getSingleton().integrateVersionData("H:/Papers-Talks-Infos/Own/Ontology Version Diff/Annotation-Example/integration.sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("H:/Papers-Talks-Infos/Own/Ontology Version Diff/Annotation-Example/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("H:/Papers-Talks-Infos/Own/Ontology Version Diff/Annotation-Example/Rule_Ensembl.xml");
		DiffExecutor.getSingleton().computeBasicChanges(false);
		DiffExecutor.getSingleton().applyRules();
		DiffExecutor.getSingleton().mergeResultActions();
		DiffExecutor.getSingleton().reduceResultActions();*/
		
		DiffExecutor.getSingleton().integrateVersionData("H:/Papers-Talks-Infos/Own/Ontology Version Diff/Annotation-Example/integrationNew.sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("H:/Papers-Talks-Infos/Own/Ontology Version Diff/Annotation-Example/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("H:/Papers-Talks-Infos/Own/Ontology Version Diff/Annotation-Example/Rule_Ensembl.xml");
		DiffExecutor.getSingleton().applyRules();
		DiffExecutor.getSingleton().mergeResultActions();
		DiffExecutor.getSingleton().retrieveAndStoreHighLevelActions();
	}
	
	public static void doMD5Example() {
		System.out.println(Utils.MD5("mergeExisENSP00000341094#ENSP00000352627ENSP00000352627"));
	}
	
	public static void doDMozSoccerExample() {
		DiffExecutor.getSingleton().integrateVersionData("H:/Papers-Talks-Infos/Own/Ontology Version Diff/DMoz-Example-Soccer/integration.sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("H:/Papers-Talks-Infos/Own/Ontology Version Diff/DMoz-Example-Soccer/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("H:/Papers-Talks-Infos/Own/Ontology Version Diff/DMoz-Example-Soccer/Rule_DMoz.xml");
		DiffExecutor.getSingleton().applyRules();
		DiffExecutor.getSingleton().mergeResultActions();
		DiffExecutor.getSingleton().reduceResultActions();
	}
	
	public static void doDMozMidEuropeExample() {
		DiffExecutor.getSingleton().integrateVersionData("H:/Papers-Talks-Infos/Own/Ontology Version Diff/DMoz-Example-MidEurope/integration.sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("H:/Papers-Talks-Infos/Own/Ontology Version Diff/DMoz-Example-MidEurope/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("H:/Papers-Talks-Infos/Own/Ontology Version Diff/DMoz-Example-MidEurope/Rule_DMoz.xml");
		DiffExecutor.getSingleton().applyRules();
		DiffExecutor.getSingleton().mergeResultActions();
		DiffExecutor.getSingleton().reduceResultActions();
	}
	
	public static void testSomeThings() {
		/*ChangeAction a1 = new ChangeAction("delAttribute",1,false);
		a1.addParamVariable(1, "A_1");
		a1.addParamVariable(2, "V_1"); 
		ChangeAction a2 = new ChangeAction("addAttribute",2,false);
		a2.addParamVariable(1, "A_2");
		a2.addParamVariable(2, "V_2");
		
		Equation e1 = new Equation("A_1.accession","A_2.accession",false,false,a1,a2);
		Equation e2 = new Equation("A_1.name","obsolete",false,true,a1,null);
		Equation e3 = new Equation("A_2.name","obsolete",false,true,a2,null);
		Equation e4 = new Equation("V_1.value","false",false,true,a1,null);
		Equation e5 = new Equation("V_2.value","true",false,true,a2,null);
		
		Rule r = new Rule();
		r.addInputAction(a1);r.addInputAction(a2);
		r.addConstraint(e1);r.addConstraint(e2);r.addConstraint(e3);r.addConstraint(e4);r.addConstraint(e5);
		
		System.out.println(r.getSQLStatement());
		
		ChangeAction a1 = new ChangeAction("addAttribute",1,false,new String[]{"C_1","A_1","V_1"});
		ChangeAction a2 = new ChangeAction("delConcept",2,false,new String[]{"C_2"});
		ChangeAction a3 = new ChangeAction("addConcept",3,true,new String[]{"C_3"});
		
		
		Equation e1 = new Equation("C_1","C_3",false,false);
		Equation e2 = new Equation("A_1","synonym",false,true);
		Equation e3 = new Equation("V_1","C_2",false,false);
		
		ResultAction r1 = new ResultAction("mergeExis",new String[]{"C_2","C_1"});
		
		Rule r = new Rule();
		r.addInputAction(a1);r.addInputAction(a2);r.addInputAction(a3);
		r.addConstraint(e1);r.addConstraint(e2);r.addConstraint(e3);
		r.setResultAction(r1);
		r.computeSQLStatement();
		
		System.out.println(r.sqlStatement);
		
		RuleParser p = new RuleParser("H:/Papers-Talks-Infos/Own/Ontology Version Diff/Rule.xml");
		p.importRules();
		
		for (int i=0;i<p.rulesToImport.size();i++) {
			p.rulesToImport.get(i).computeSQLStatement();
			System.out.println(p.rulesToImport.get(i).sqlStatement);
		}*/
	}
}
