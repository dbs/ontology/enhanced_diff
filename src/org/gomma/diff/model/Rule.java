package org.gomma.diff.model;

import java.util.List;
import java.util.Vector;

import org.gomma.diff.Globals;


public class Rule {
	public List<InputAction> inputActions;
	public List<Equation> constraints;
	public ResultAction resultAction;
	public String fullActionSQLStatement;
	public String resultActionSQLStatement;
	public List<InputAction> reduceInputActions;
	public List<String> reduceActionSQLStatements;
	public int applyUntilRound = Integer.MAX_VALUE;
	
	public Rule() {
		this.inputActions = new Vector<InputAction>();
		this.constraints = new Vector<Equation>();
		this.reduceInputActions = new Vector<InputAction>();
		this.reduceActionSQLStatements = new Vector<String>();
	}
	
	public void addInputAction(InputAction action) {
		this.inputActions.add(action);
	}
	
	public void addConstraint(Equation eq) {
		this.constraints.add(eq);
	}
	public void setResultAction(ResultAction action) {
		this.resultAction = action;
	}
	public void addReduceInputAction(int actionPos) {
		this.reduceInputActions.add(this.inputActions.get(actionPos-1));
	}
	
	public List<InputAction> getCompositeInputActions() {
		List<InputAction> result = new Vector<InputAction>();
		for (InputAction inputAction: this.inputActions) {
			if (!inputAction.negation&&!inputAction.checkOnly) {
				result.add(inputAction);
			}
		}
		return result;
	}

	public void buildSQLStatements() {
		StringBuffer result = new StringBuffer();
		String mainSQLBody = this.buildSQLBody();
		
		//SQL f�r ResultAction
		if (this.resultAction.resultVariables.size()>0) {	
			result.append(this.buildResultActionSQLHead());
			result.append(mainSQLBody);
			this.resultActionSQLStatement = result.toString();
		}
		
		//SQL f�r Reduce
		for (int i=0;i<this.reduceInputActions.size();i++) {
			result = new StringBuffer();
			InputAction tmpAction = this.reduceInputActions.get(i);
			result.append(this.buildReduceActionSQLHead(tmpAction));
			result.append(mainSQLBody);
			this.reduceActionSQLStatements.add(result.toString());
		}
		
		//Full SQL
		if (this.resultAction.resultVariables.size()>0) {
			this.fullActionSQLStatement = buildFullActionSQLHead()+mainSQLBody;
		}
	}

	private String buildFullActionSQLHead() {
		StringBuffer result = new StringBuffer();
		result.append("SELECT ");
		List<String> outputVariables = this.resultAction.getResultVariables();
		for (int i=0;i<outputVariables.size();i++) {
			String outputVariable = outputVariables.get(i);
			InputAction responsibleAction = this.getChangeAction(outputVariable);
			int valuePos = responsibleAction.getValueNumber(outputVariable);
			result.append("t"+responsibleAction.position+".value"+valuePos+",");
		}
		for (int i=0;i<inputActions.size();i++) {
			InputAction changeAction = inputActions.get(i);
			if (!changeAction.negation&&!changeAction.checkOnly) {
				outputVariables = changeAction.paramVariables;
				for (int j=0;j<outputVariables.size();j++) {
					String outputVariable = outputVariables.get(j);
					int valuePos = changeAction.getValueNumber(outputVariable);
					result.append("t"+changeAction.position+".value"+valuePos+",");
				}
			}
		}
		result = result.delete(result.length()-1, result.length());
		return result.toString();
	}
	
	private String buildResultActionSQLHead() {
		StringBuffer result = new StringBuffer();
		result.append("SELECT ");
		List<String> outputVariables = this.resultAction.getResultVariables();
		for (int i=0;i<outputVariables.size();i++) {
			String outputVariable = outputVariables.get(i);
			InputAction responsibleAction = this.getChangeAction(outputVariable);
			int valuePos = responsibleAction.getValueNumber(outputVariable);
			result.append("t"+responsibleAction.position+".value"+valuePos+",");
		}
		result = result.delete(result.length()-1, result.length());
		return result.toString();
	}
	private String buildReduceActionSQLHead(InputAction changeAction) {
		StringBuffer result = new StringBuffer();
		result.append("SELECT ");
		List<String> outputVariables = changeAction.paramVariables;
		for (int i=0;i<outputVariables.size();i++) {
			String outputVariable = outputVariables.get(i);
			int valuePos = changeAction.getValueNumber(outputVariable);
			result.append("t"+changeAction.position+".value"+valuePos+",");
		}
		result = result.delete(result.length()-1, result.length());
		return result.toString();
	}
	
	private String buildSQLBody() {
		List<Boolean> constraintsIncluded = new Vector<Boolean>();
		for (int i=0;i<this.constraints.size();i++) {
			constraintsIncluded.add(false);
		}
		
		StringBuffer result = new StringBuffer();
		result.append(" FROM ");
		StringBuffer tmpChanges = new StringBuffer();

		//Auflistung aller Eingabe-Tupel
		for (int i=0;i<inputActions.size();i++) {
			InputAction currentAction = inputActions.get(i);
			if (!currentAction.negation) {
				result.append(Globals.WORKING_TABLE+" t"+(i+1)+",");
				tmpChanges.append("t"+(i+1)+".change_action='"+currentAction.actionDesc.name+"' AND ");
			} else if (currentAction.negation) {
				tmpChanges.append("NOT EXISTS (SELECT * FROM "+Globals.WORKING_TABLE+" t"+(i+1)+" WHERE  t"+(i+1)+".change_action = '"+currentAction.actionDesc.name+"' ");
				for (int j=0;j<this.constraints.size();j++) {
					Equation tmpEq = this.constraints.get(j);
					if (tmpEq.belongsToAction(currentAction)) {
						if (!tmpEq.leftSideConstant) {
							InputAction leftAction = this.getChangeAction(tmpEq.leftSide);
							int valuePos = leftAction.getValueNumber(tmpEq.leftSide);
							tmpChanges.append("AND t"+leftAction.position+".value"+valuePos);
						} else {
							tmpChanges.append("AND '"+tmpEq.leftSide.replace("\"", "\"\"")+"'");
						}
						
						if (tmpEq.equationType.equalsIgnoreCase(Globals.EQUATION_IN_SET)) {
							tmpChanges.append(" IN ("+tmpEq.rightSide.replace("\"", "\"\"")+") ");
						} else {
							if (tmpEq.equationType.equalsIgnoreCase(Globals.EQUATION_UNEQUAL)) {
								tmpChanges.append("!=");
							} else {
								tmpChanges.append("=");
							}
							if (!tmpEq.rightSideConstant) {
								InputAction rightAction = this.getChangeAction(tmpEq.rightSide);
								int valuePos = rightAction.getValueNumber(tmpEq.rightSide);
								tmpChanges.append("t"+rightAction.position+".value"+valuePos+" ");
							} else {
								tmpChanges.append("'"+tmpEq.rightSide.replace("\"", "\"\"")+"' ");
							}
						}
						constraintsIncluded.set(j, true);
					}
				}
				tmpChanges.append(") AND ");
			}
		}
		result = result.delete(result.length()-1, result.length());
		result.append(" WHERE "+tmpChanges);
		
		for (int i=0;i<this.constraints.size();i++) {
			if (!constraintsIncluded.get(i)) {
				Equation tmpEq = this.constraints.get(i);
				if (!tmpEq.leftSideConstant) {
					InputAction leftAction = this.getChangeAction(tmpEq.leftSide);
					int valuePos = leftAction.getValueNumber(tmpEq.leftSide);
					result.append("t"+leftAction.position+".value"+valuePos);
				} else {
					result.append("'"+tmpEq.leftSide.replace("\"", "\"\"")+"'");
				}
				
				if (tmpEq.equationType.equalsIgnoreCase(Globals.EQUATION_IN_SET)) {
					result.append(" IN ("+tmpEq.rightSide.replace("\"", "\"\"")+") ");
				} else {				
					if (tmpEq.equationType.equalsIgnoreCase("unequal")) {
						result.append("!=");
					} else {
						result.append("=");
					}
					if (!tmpEq.rightSideConstant) {
						InputAction rightAction = this.getChangeAction(tmpEq.rightSide);
						int valuePos = rightAction.getValueNumber(tmpEq.rightSide);
						result.append("t"+rightAction.position+".value"+valuePos);
					} else {
						result.append("'"+tmpEq.rightSide.replace("\"", "\"\"")+"'");
					}
				}
				result.append(" AND ");
				constraintsIncluded.set(i, true);
			}
		}
		result = result.delete(result.length()-5, result.length());
		return result.toString();
	}
	
	private InputAction getChangeAction(String variableName) {
		for (int i=0;i<inputActions.size();i++) {
			InputAction tmp = inputActions.get(i);
			if (tmp.paramVariables.contains(variableName)) {
				return tmp;
			}
		}
		return null;
	}
}
