package org.gomma.diff.model;

import java.util.List;
import java.util.Vector;


public class ActionDesc {
	public int id;
	public String name;
	public int level;
	public List<String> paramTypes;
	public List<Boolean> multipleValues;
	
	public ActionDesc() {
		this.paramTypes = new Vector<String>();
		this.multipleValues = new Vector<Boolean>();
	}
	
	public ActionDesc(int id,String name,int level,List<String> paramTypes,List<Boolean> multipleValues) {
		this.id = id;
		this.name = name;
		this.paramTypes = paramTypes;
		this.multipleValues = multipleValues;
	}
	
	public boolean isMergeable() {
		for (int i=0;i<multipleValues.size();i++) {
			if (multipleValues.get(i)) {
				return true;
			}
		}
		return false;
	}
}
