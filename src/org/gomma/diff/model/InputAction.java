package org.gomma.diff.model;

import java.util.List;
import java.util.Vector;

import org.gomma.diff.DiffExecutor;


public class InputAction {
	public ActionDesc actionDesc;
	public List<String> paramVariables;
	public int position;
	public boolean negation = false;
	public boolean checkOnly = false;
	
	public InputAction() {
		this.paramVariables = new Vector<String>();
	}
	
	public InputAction(String name, int position, boolean negation) {
		this.actionDesc = DiffExecutor.getSingleton().getChangeActionDesc(name);
		this.position = position;
		this.negation = negation;
		this.paramVariables = new Vector<String>();
	}
	public InputAction(String name, int position, boolean negation, List<String> paramVariables) {
		this(name,position,negation);
		this.paramVariables = paramVariables;
	}
	public InputAction(String name, int position, boolean negation, String[] paramVariables) {
		this(name,position,negation);
		for (int i=0;i<paramVariables.length;i++) {
			this.paramVariables.add(paramVariables[i]);
		}
	}
	
	public void addParamVariable(String variable) {
		this.paramVariables.add(variable);
	}
	
	public int getValueNumber(String varName) {
		int pos = -1;
		for (int i=0;i<paramVariables.size();i++) {
			if (paramVariables.get(i).equals(varName)) {
				pos = i+1;
			}
		}
		return pos;
	}
}
