package org.gomma.diff.model;

import org.gomma.diff.Globals;


public class Equation {
	public String rightSide;
	public String leftSide;
	public String equationType = Globals.EQUATION_EQUAL;
	public boolean leftSideConstant = false;
	public boolean rightSideConstant = false;
	
	public Equation() {
	}
	
	public Equation(String leftSide, String rightSide) {
		this.leftSide = leftSide;
		this.rightSide = rightSide;
	}
	
	public Equation(String leftSide, String rightSide, boolean leftSideConstant, boolean rightSideConstant) {
		this.leftSide = leftSide;
		this.rightSide = rightSide;
		this.leftSideConstant = leftSideConstant;
		this.rightSideConstant = rightSideConstant;
	}
	
	public Equation(String leftSide, String rightSide, boolean leftSideConstant, boolean rightSideConstant, String equationType) {
		this.leftSide = leftSide;
		this.rightSide = rightSide;
		this.leftSideConstant = leftSideConstant;
		this.rightSideConstant = rightSideConstant;
		this.equationType = equationType;
	}
	
	public boolean containsVariable(String variable) {
		if (leftSide.contains(variable)||rightSide.contains(variable)) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean belongsToAction(InputAction action) {
		for (int i=0;i<action.paramVariables.size();i++) {
			if (this.containsVariable(action.paramVariables.get(i))) {
				return true;
			}
		}
		return false;
	}
}
