package org.gomma.diff.model;

import java.util.List;
import java.util.Vector;

import org.gomma.diff.utils.Utils;

public class ActionData {
	public ActionDesc changeActionDesc;
	public List<String> dataValues;
	public String md5Key;
	public List<String> derivedFrom;
	
	public ActionData(ActionDesc changeActionDesc) {
		this.changeActionDesc = changeActionDesc;
		this.dataValues = new Vector<String>();
		this.derivedFrom = new Vector<String>();
	}
 	
	public void addDataValue(String value) {
		this.dataValues.add(value);
	}
	
	public boolean checkCorrectness() {
		if (this.dataValues.size()==this.changeActionDesc.paramTypes.size()) {
			return true;
		}
		return false;
	}
	
	public boolean equals(Object o) {
		if (o instanceof ActionData) {
			ActionData tmpObj = (ActionData)o;
			if (tmpObj.changeActionDesc!=this.changeActionDesc) {
				return false;
			}
			if (tmpObj.dataValues.size()==this.dataValues.size()) {
				for (int i=0;i<this.dataValues.size();i++) {
					if (!this.dataValues.get(i).equals(tmpObj.dataValues.get(i))) {
						return false;
					}
				}
				return true;
			}
		}
		return false;
	}
	
	public String toString() {
		StringBuffer result = new StringBuffer();
		result.append(this.changeActionDesc.name);
		for (int i=0;i<this.dataValues.size();i++) {
			result.append("\t"+this.dataValues.get(i));
		}
		return result.toString();
	}
	
	public String getMD5() {
		if (this.md5Key==null) {
			StringBuffer result = new StringBuffer(this.changeActionDesc.name);
			for (int i=0;i<dataValues.size();i++) {
				result.append(dataValues.get(i));
			}
			this.md5Key = Utils.MD5(result.toString());
		}
		return this.md5Key;
	}
	
	public void computeDerivedFrom(String derivedFromAsString) {
		String[] allDerivedFrom = derivedFromAsString.split("#");
		if (allDerivedFrom.length>1) {
			for (int i=0;i<allDerivedFrom.length;i++) {
				this.derivedFrom.add(allDerivedFrom[i].trim());
			}
		} else {
			if (!this.getMD5().equals(derivedFromAsString.trim())) {
				this.derivedFrom.add(derivedFromAsString.trim());
			}
		}
	}
	
	public boolean isDerivedFrom() {
		if (this.derivedFrom.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
}
