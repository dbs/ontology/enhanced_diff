package org.gomma.diff.model;

import java.util.List;
import java.util.Vector;

import org.gomma.diff.DiffExecutor;


public class ResultAction {
	public ActionDesc actionDesc;
	public List<String> resultVariables;
	
	public ResultAction() {
		this.resultVariables = new Vector<String>();
	}
	
	public ResultAction(String name) {
		this.actionDesc = DiffExecutor.getSingleton().getChangeActionDesc(name);
		this.resultVariables = new Vector<String>();
	}
	public ResultAction(String name, List<String> resultVariables) {
		this(name); 
		this.resultVariables = resultVariables;
		
	}
	public ResultAction(String name, String[] resultVariables) {
		this(name);
		for (int i=0;i<resultVariables.length;i++) {
			this.resultVariables.add(resultVariables[i]);
		}
	}
	
	public List<String> getResultVariables() {
		return this.resultVariables;
	}
	
	public void addParamVariable(String variable) {
		this.resultVariables.add(variable);
	}
}
