package org.gomma.diff.model;

import java.util.List;
import java.util.Vector;

public class Type {
	public String name;
	public String baseType;
	public List<String> containedPrimTypes;
	
	public Type() {
		this.containedPrimTypes = new Vector<String>();
	}
	
	public Type(String name, String baseType) {
		this.name = name;
		this.baseType = baseType;
		containedPrimTypes = new Vector<String>();
	}
	
	public Type(String name, String baseType, List<String> primTypes) {
		this.name = name;
		this.baseType = baseType;
		this.containedPrimTypes = primTypes;
	}
	
	public void addPrimType(String primType) {
		this.containedPrimTypes.add(primType);
	}
}
