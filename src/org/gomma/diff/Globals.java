package org.gomma.diff;

public class Globals {
	//DB connection data
	public static final String DB_URL = "jdbc:mysql://dbserv2.informatik.uni-leipzig.de:3306/web_diff_tool?autoReconnect=true";
	public static final String DB_USER = "web_diff_tool";
	public static final String DB_PW = "COntoDiff";
	
	//Delimiter for multiple values
	public static String DELIMITER = "#";
	
	//Equation types
	public static String EQUATION_EQUAL = "equal";
	public static String EQUATION_UNEQUAL = "unequal";
	public static String EQUATION_IN_SET = "in";
	
	//Schema information
	public static String VERSION_TABLE_BASE = "evol_versions";
	public static String VERSION_TABLE = "evol_versions";
	public static String WORKING_TABLE_BASE = "evol_change";
	public static String WORKING_TABLE = "evol_change";
	public static String HIGH_LEVEL_RESULT_TABLE_BASE = "high_level_actions";
	public static String HIGH_LEVEL_RESULT_TABLE = "high_level_actions";
	public static String LOW_LEVEL_RESULT_TABLE_BASE = "low_level_actions";
	public static String LOW_LEVEL_RESULT_TABLE = "low_level_actions";
	public static String HIGH_TO_LOW_LEVEL_MAPPING_TABLE_BASE = "high_to_low_mapping";
	public static String HIGH_TO_LOW_LEVEL_MAPPING_TABLE = "high_to_low_mapping";
	
	public static String VERSION_TABLE_SCHEMA = "CREATE TABLE IF NOT EXISTS "+VERSION_TABLE+" ("+
													  "version varchar(20) NOT NULL,"+
													  "type varchar(50) NOT NULL,"+
													  "base_type varchar(50) NOT NULL,"+
													  "values_md5 varchar(50) NOT NULL,"+
													  "value1 varchar(2000) default NULL,"+
													  "value2 varchar(2000) default NULL,"+
													  "value3 varchar(2000) default NULL,"+
													  "value4 varchar(2000) default NULL,"+
													  "value5 varchar(2000) default NULL,"+
													  "value6 varchar(2000) default NULL,"+
													  "KEY version (version,base_type,type),"+
													  "KEY value1 (value1(1000)),"+
													  "KEY value2 (value2(1000)),"+
													  "KEY value3 (value3(1000)),"+
													  "KEY value4 (value4(1000)),"+
													  "KEY value5 (value5(1000)),"+
													  "KEY value6 (value6(1000)),"+
													  "KEY version_2 (version,type,base_type,values_md5),"+
													  "KEY values_md5 (values_md5))";
	public static String WORKING_TABLE_SCHEMA = "CREATE TABLE IF NOT EXISTS "+WORKING_TABLE+" ("+
													  "actionMD5 varchar(50) default NULL,"+
													  "change_action varchar(50) NOT NULL,"+
													  "reduce int(11) default '0',"+
													  "value1 varchar(10000) default NULL,"+
													  "value2 varchar(10000) default NULL,"+
													  "value3 varchar(10000) default NULL,"+
													  "value4 varchar(10000) default NULL,"+
													  "value5 varchar(10000) default NULL,"+
													  "value6 varchar(10000) default NULL,"+
													  "UNIQUE KEY actionMD5 (actionMD5),"+
													  "KEY change_action (change_action),"+
													  "KEY reduce (reduce),"+
													  "KEY value1 (value1(1000)),"+
													  "KEY value2 (value2(1000)),"+
													  "KEY value3 (value3(1000)),"+
													  "KEY value4 (value4(1000)),"+
													  "KEY value5 (value5(1000)),"+
													  "KEY value6 (value6(1000)))";
	public static String HIGH_LEVEL_RESULT_SCHEMA = "CREATE TABLE IF NOT EXISTS "+HIGH_LEVEL_RESULT_TABLE+" ("+
													  "actionMD5 varchar(50) default NULL,"+
													  "change_action varchar(50) NOT NULL,"+
													  "value1 varchar(10000) default NULL,"+
													  "value2 varchar(10000) default NULL,"+
													  "value3 varchar(10000) default NULL,"+
													  "value4 varchar(10000) default NULL,"+
													  "value5 varchar(10000) default NULL,"+
													  "value6 varchar(10000) default NULL,"+
													  "UNIQUE KEY actionMD5 (actionMD5),"+
													  "KEY change_action (change_action),"+
													  "KEY value1 (value1(1000)),"+
													  "KEY value2 (value2(1000)),"+
													  "KEY value3 (value3(1000)),"+
													  "KEY value4 (value4(1000)),"+
													  "KEY value5 (value5(1000)),"+
													  "KEY value6 (value6(1000)))";
	public static String LOW_LEVEL_RESULT_SCHEMA = "CREATE TABLE IF NOT EXISTS "+LOW_LEVEL_RESULT_TABLE+" ("+
													  "actionMD5 varchar(50) default NULL,"+
													  "change_action varchar(50) NOT NULL,"+
													  "value1 varchar(10000) default NULL,"+
													  "value2 varchar(10000) default NULL,"+
													  "value3 varchar(10000) default NULL,"+
													  "value4 varchar(10000) default NULL,"+
													  "value5 varchar(10000) default NULL,"+
													  "value6 varchar(10000) default NULL,"+
													  "UNIQUE KEY actionMD5 (actionMD5),"+
													  "KEY change_action (change_action),"+
													  "KEY value1 (value1(1000)),"+
													  "KEY value2 (value2(1000)),"+
													  "KEY value3 (value3(1000)),"+
													  "KEY value4 (value4(1000)),"+
													  "KEY value5 (value5(1000)),"+
													  "KEY value6 (value6(1000)))";
	public static String HIGH_TO_LOW_LEVEL_MAPPING_SCHEMA = "CREATE TABLE "+HIGH_TO_LOW_LEVEL_MAPPING_TABLE+" ("+
													  "high_level_action_MD5 varchar(50) NOT NULL,"+
													  "low_level_action_MD5 varchar(50) NOT NULL,"+
													  "KEY high_level_action_MD5 (high_level_action_MD5),"+
													  "KEY low_level_action_MD5 (low_level_action_MD5))";
	
	public static void addPrefix(String prefix) {
		VERSION_TABLE = prefix + "_" + VERSION_TABLE_BASE;
		WORKING_TABLE = prefix + "_" + WORKING_TABLE_BASE;
		HIGH_LEVEL_RESULT_TABLE = prefix + "_" + HIGH_LEVEL_RESULT_TABLE_BASE;
		LOW_LEVEL_RESULT_TABLE = prefix + "_" + LOW_LEVEL_RESULT_TABLE_BASE;
		HIGH_TO_LOW_LEVEL_MAPPING_TABLE = prefix + "_" + HIGH_TO_LOW_LEVEL_MAPPING_TABLE_BASE;
		
		VERSION_TABLE_SCHEMA = "CREATE TABLE IF NOT EXISTS "+VERSION_TABLE+" ("+
		  "version varchar(20) NOT NULL,"+
		  "type varchar(50) NOT NULL,"+
		  "base_type varchar(50) NOT NULL,"+
		  "values_md5 varchar(50) NOT NULL,"+
		  "value1 varchar(2000) default NULL,"+
		  "value2 varchar(2000) default NULL,"+
		  "value3 varchar(2000) default NULL,"+
		  "value4 varchar(2000) default NULL,"+
		  "value5 varchar(2000) default NULL,"+
		  "value6 varchar(2000) default NULL,"+
		  "KEY version (version,base_type,type),"+
		  "KEY value1 (value1(1000)),"+
		  "KEY value2 (value2(1000)),"+
		  "KEY value3 (value3(1000)),"+
		  "KEY value4 (value4(1000)),"+
		  "KEY value5 (value5(1000)),"+
		  "KEY value6 (value6(1000)),"+
		  "KEY version_2 (version,type,base_type,values_md5),"+
		  "KEY values_md5 (values_md5))";
		WORKING_TABLE_SCHEMA = "CREATE TABLE IF NOT EXISTS "+WORKING_TABLE+" ("+
		  "actionMD5 varchar(50) default NULL,"+
		  "change_action varchar(50) NOT NULL,"+
		  "reduce int(11) default '0',"+
		  "value1 varchar(10000) default NULL,"+
		  "value2 varchar(10000) default NULL,"+
		  "value3 varchar(10000) default NULL,"+
		  "value4 varchar(10000) default NULL,"+
		  "value5 varchar(10000) default NULL,"+
		  "value6 varchar(10000) default NULL,"+
		  "UNIQUE KEY actionMD5 (actionMD5),"+
		  "KEY change_action (change_action),"+
		  "KEY reduce (reduce),"+
		  "KEY value1 (value1(1000)),"+
		  "KEY value2 (value2(1000)),"+
		  "KEY value3 (value3(1000)),"+
		  "KEY value4 (value4(1000)),"+
		  "KEY value5 (value5(1000)),"+
		  "KEY value6 (value6(1000)))";
		HIGH_LEVEL_RESULT_SCHEMA = "CREATE TABLE IF NOT EXISTS "+HIGH_LEVEL_RESULT_TABLE+" ("+
		  "actionMD5 varchar(50) default NULL,"+
		  "change_action varchar(50) NOT NULL,"+
		  "value1 varchar(10000) default NULL,"+
		  "value2 varchar(10000) default NULL,"+
		  "value3 varchar(10000) default NULL,"+
		  "value4 varchar(10000) default NULL,"+
		  "value5 varchar(10000) default NULL,"+
		  "value6 varchar(10000) default NULL,"+
		  "UNIQUE KEY actionMD5 (actionMD5),"+
		  "KEY change_action (change_action),"+
		  "KEY value1 (value1(1000)),"+
		  "KEY value2 (value2(1000)),"+
		  "KEY value3 (value3(1000)),"+
		  "KEY value4 (value4(1000)),"+
		  "KEY value5 (value5(1000)),"+
		  "KEY value6 (value6(1000)))";
		LOW_LEVEL_RESULT_SCHEMA = "CREATE TABLE IF NOT EXISTS "+LOW_LEVEL_RESULT_TABLE+" ("+
		  "actionMD5 varchar(50) default NULL,"+
		  "change_action varchar(50) NOT NULL,"+
		  "value1 varchar(10000) default NULL,"+
		  "value2 varchar(10000) default NULL,"+
		  "value3 varchar(10000) default NULL,"+
		  "value4 varchar(10000) default NULL,"+
		  "value5 varchar(10000) default NULL,"+
		  "value6 varchar(10000) default NULL,"+
		  "UNIQUE KEY actionMD5 (actionMD5),"+
		  "KEY change_action (change_action),"+
		  "KEY value1 (value1(1000)),"+
		  "KEY value2 (value2(1000)),"+
		  "KEY value3 (value3(1000)),"+
		  "KEY value4 (value4(1000)),"+
		  "KEY value5 (value5(1000)),"+
		  "KEY value6 (value6(1000)))";
		HIGH_TO_LOW_LEVEL_MAPPING_SCHEMA = "CREATE TABLE "+HIGH_TO_LOW_LEVEL_MAPPING_TABLE+" ("+
		  "high_level_action_MD5 varchar(50) NOT NULL,"+
		  "low_level_action_MD5 varchar(50) NOT NULL,"+
		  "KEY high_level_action_MD5 (high_level_action_MD5),"+
		  "KEY low_level_action_MD5 (low_level_action_MD5))";
	}
}
