package org.gomma.diff;

import org.gomma.diff.utils.*;

public class DiffTester_GO {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception  {
		long start = System.currentTimeMillis();

		//echte 12 Jahresvergleich 2008-2009 GO-CC/MF/BP
		String ontology 		= "GO-BP";
		int lds 				= 2;//1,2,4
		String start_version	= "01-2009";	//01-2006//01-2007//01-2008//01-2009
		String start_version_db = "2009-01-15";	//2006-01-15//2007-01-15//2008-01-15//2009-01-15
		String end_version	  	= "01-2010";	//12-2006//12-2007//12-2008//12-2009
		String end_version_db 	= "2010-01-15";	//2006-12-15//2007-12-15//2008-12-15//2009-12-15
		System.out.println("Start querying for basic match ... ");
		GOBasicMatch.computeBasicDiff(lds, start_version_db, end_version_db, "E:/workspace/enhancedDiff/GeneOntology-Evaluation/"+ontology+"/"+start_version+"_"+end_version+"/baseMatch_"+ontology+"_"+start_version+"_"+end_version+".txt");
		System.out.println("Done in "+(System.currentTimeMillis()-start)+" ms\n");
		
	/*
		//2 Jahresvergleich 2008-2009 GO-CC
		String ontology 		= "GO-CC";
		int lds 				= 4;
		String start_version	= "01-2009";	//01-2006//01-2007//01-2008//01-2009
		String start_version_db = "2009-01-15";	//2006-01-15//2007-01-15//2008-01-15//2009-01-15
		String end_version	  	= "01-2010";	//12-2006//12-2007//12-2008//12-2009
		String end_version_db 	= "2010-01-15";	//2006-12-15//2007-12-15//2008-12-15//2009-12-15
		System.out.println("Start querying for basic match ... ");
		GOBasicMatch.computeBasicDiff(lds, start_version_db, end_version_db, "E:/workspace/enhancedDiff/GeneOntology-Evaluation/"+ontology+"/"+start_version+"_"+end_version+"/baseMatch_"+ontology+"_"+start_version+"_"+end_version+".txt");
		System.out.println("Done in "+(System.currentTimeMillis()-start)+" ms\n");
		*/
		//doGOExample(ontology,start_version,end_version);
			
		
		//4 Jahresvergleich 2006-2009 GO-MF
		/*			
		 * String ontology 		= "GO-MF";
		int lds 				= 1;
		String start_version	= "01-2009";	//01-2006//01-2007//01-2008//01-2009
		String start_version_db = "2009-01-15";	//2006-01-15//2007-01-15//2008-01-15//2009-01-15
		String end_version	  	= "12-2009";	//12-2006//12-2007//12-2008//12-2009
		String end_version_db 	= "2009-12-15";	//2006-12-15//2007-12-15//2008-12-15//2009-12-15
		System.out.println("Start querying for basic match ... ");
		//GOBasicMatch.computeBasicDiff(lds, start_version_db, end_version_db, "E:/workspace/enhancedDiff/GeneOntology-Evaluation/"+ontology+"/"+start_version+"_"+end_version+"/baseMatch_"+ontology+"_"+start_version+"_"+end_version+".txt");
		System.out.println("Done in "+(System.currentTimeMillis()-start)+" ms\n");
		
		doGOExample(ontology,start_version,end_version);


		//4 Jahresvergleich 2006-2009 GO-BP
		String ontology 		= "GO-BP";
		int lds 				= 2;
		String start_version	= "01-2008";	//01-2006//01-2007//01-2008//01-2009
		String start_version_db = "2008-01-15";	//2006-01-15//2007-01-15//2008-01-15//2009-01-15
		String end_version	  	= "12-2008";	//12-2006//12-2007//12-2008//12-2009
		String end_version_db 	= "2008-12-15";	//2006-12-15//2007-12-15//2008-12-15//2009-12-15
		System.out.println("Start querying for basic match ... ");
		//GOBasicMatch.computeBasicDiff(lds, start_version_db, end_version_db, "E:/workspace/enhancedDiff/GeneOntology-Evaluation/"+ontology+"/"+start_version+"_"+end_version+"/baseMatch_"+ontology+"_"+start_version+"_"+end_version+".txt");
		System.out.println("Done in "+(System.currentTimeMillis()-start)+" ms\n");
		
		doGOExample(ontology,start_version,end_version);
		*/
		
		/*
		//QUARTALE 2008
		String ontology 		= "GO-MF";
		int lds 				= 1;
		String start_version	= "01-2008";
		String start_version_db = "2008-01-15";	
		String end_version	  	= "09-2008";//12-2008//09-2008//06-2008//03-2008
		String end_version_db 	= "2008-09-15";//2008-12-15//2008-06-15//2008-06-15//2008-03-15
		System.out.println("Start querying for basic match ... ");
		GOBasicMatch.computeBasicDiff(lds, start_version_db, end_version_db, "E:/workspace/enhancedDiff/GeneOntology-Evaluation/"+ontology+"/"+start_version+"_"+end_version+"/baseMatch_"+ontology+"_"+start_version+"_"+end_version+".txt");
		System.out.println("Done in "+(System.currentTimeMillis()-start)+" ms\n");
		doGOExample(ontology,start_version,end_version);
		*/
		//DONE
		//doGOExample("GO-MF","01-2008","12-2008");
		//doGOExample("GO-BP","05-2007","06-2007");
		System.out.println("-------\nOverall elapsed time: "+(System.currentTimeMillis()-start)+" ms");
		
	}
	
	
	public static void doGOExample(String ontology,String start_version,String end_version) throws Exception {
		DataBaseHandler.getInstance().createDatabaseConnection(Globals.DB_URL,Globals.DB_USER,Globals.DB_PW);
		//DiffExecutor.getSingleton().setupRepository();
		DiffExecutor.getSingleton().integrateVersionData("E:/workspace/enhancedDiff/GeneOntology-Evaluation/"+ontology+"/"+start_version+"_"+end_version+"/integration_"+ontology+"_"+start_version+"_"+end_version+".sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("E:/workspace/enhancedDiff/GeneOntology-Evaluation/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("E:/workspace/enhancedDiff/GeneOntology-Evaluation/Rule_GO.xml");
		DiffExecutor.getSingleton().applyRules();
		DiffExecutor.getSingleton().mergeResultActions();
		DiffExecutor.getSingleton().retrieveAndStoreHighLevelActions();
		
	}	
}
