package org.gomma.diff.match;

public class MatchGlobals {
	//What is matched?
	public final static int OBJ_OBJ_MATCH = 1;
	public final static int OBJ_ATT_MATCH = 2;
	public final static int OBJ_ASS_MATCH = 3;
	
	public final static int ATT_OBJ_MATCH = 4;
	public final static int ATT_ATT_MATCH = 5;
	public final static int ATT_ASS_MATCH = 6;
	
	public final static int ASS_OBJ_MATCH = 7;
	public final static int ASS_ATT_MATCH = 8;
	public final static int ASS_ASS_MATCH = 9;
	
	
	//Match Method to be used
	public final static int FULL_EXACT_MATCH = 1;
	public final static int TYPE_EXACT_MATCH = 2;
	public final static int VALUE_EXACT_MATCH = 3;
}
