package org.gomma.diff.match;

import java.util.Properties;

public class MatchConfig {
	public int matchType;
	public int matchMethod;
	public Properties props;
	
	public MatchConfig(int matchType,int matchMethod,Properties props) {
		this.matchType = matchType;
		this.matchMethod = matchMethod;
		this.props = props;
	}
}
