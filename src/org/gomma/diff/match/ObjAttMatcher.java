package org.gomma.diff.match;

import java.sql.SQLException;
import java.util.Properties;

import org.gomma.diff.utils.DataBaseHandler;


public class ObjAttMatcher {
	public int matchMethod;
	public Properties props;
	
	public ObjAttMatcher(int matchMethod,Properties props) {
		this.matchMethod = matchMethod;
		this.props = props;
	}
	
	public void execute() {
		switch (this.matchMethod) {
		case MatchGlobals.VALUE_EXACT_MATCH:
			this.doValueExactMatch();
			break;
			
		default:
			break;
		}
	}
	
	private void doValueExactMatch() {
		String query = "INSERT into test_basic_diff_map (map_type,from_md5,to_md5) SELECT 'mapObjAtt',a.values_md5,b.values_md5 ";
		query += "FROM test_evol_versions a, test_evol_versions b WHERE a.base_type='Object' AND b.base_type='Attribute' AND a.version='old' AND b.version='new' AND a.value1 = b.value3 AND a.value1 != b.value1";
		try {
			DataBaseHandler.getInstance().executeDml(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
