package org.gomma.diff.match;

import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

import org.gomma.diff.utils.DataBaseHandler;


public class MatchExecutor {
	public List<MatchConfig> matchConfigs;
	
	public MatchExecutor() {
		this.matchConfigs = new Vector<MatchConfig>();
	}
	
	public MatchExecutor(List<MatchConfig> matchConfig) {
		this.matchConfigs = matchConfig;
	}
	
	public void executeBasicDiff() {
		try {
			DataBaseHandler.getInstance().executeDml("TRUNCATE test_basic_diff_map");
			this.executeMatches();
			this.insertAddDelElements();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void executeMatches() {
		for (int i=0;i<this.matchConfigs.size();i++) {
			this.executeMatch(this.matchConfigs.get(i));
		}
	}
	
	public void executeMatch(MatchConfig config) {
		switch (config.matchType) {
		case MatchGlobals.OBJ_OBJ_MATCH:
			ObjObjMatcher m1 = new ObjObjMatcher(config.matchMethod, config.props);
			m1.execute();
			break;

		case MatchGlobals.OBJ_ATT_MATCH:
			ObjAttMatcher m2 = new ObjAttMatcher(config.matchMethod, config.props);
			m2.execute();
			break;
		case MatchGlobals.ATT_ATT_MATCH:
			AttAttMatcher m3 = new AttAttMatcher(config.matchMethod, config.props);
			m3.execute();
			break;
		case MatchGlobals.ASS_ASS_MATCH:
			AssAssMatcher m4 = new AssAssMatcher(config.matchMethod, config.props);
			m4.execute();
			break;	
		default:
			break;
		}
	}
	
	public void insertAddDelElements() {
		try {
			String query = "INSERT INTO test_basic_diff_map (map_type,from_md5,to_md5) "+
							"SELECT 'mapAddObj','',b.values_md5 "+
							"FROM test_evol_versions b WHERE b.version='new' AND b.base_type='object' AND b.values_md5 NOT IN (SELECT a.from_md5 FROM test_basic_diff_map a)";
			DataBaseHandler.getInstance().executeDml(query);
			
			query = "INSERT INTO test_basic_diff_map (map_type,from_md5,to_md5) "+
					"SELECT 'mapDelObj','',b.values_md5 "+
					"FROM test_evol_versions b WHERE b.version='old' AND b.base_type='object' AND b.values_md5 NOT IN (SELECT a.from_md5 FROM test_basic_diff_map a)";
			DataBaseHandler.getInstance().executeDml(query);
			
			query = "INSERT INTO test_basic_diff_map (map_type,from_md5,to_md5) "+
					"SELECT 'mapAddAtt','',b.values_md5 "+
					"FROM test_evol_versions b WHERE b.version='new' AND b.base_type='attribute' AND b.values_md5 NOT IN (SELECT a.from_md5 FROM test_basic_diff_map a)";
			DataBaseHandler.getInstance().executeDml(query);
			
			query = "INSERT INTO test_basic_diff_map (map_type,from_md5,to_md5) "+
					"SELECT 'mapDelAtt','',b.values_md5 "+
					"FROM test_evol_versions b WHERE b.version='old' AND b.base_type='attribute' AND b.values_md5 NOT IN (SELECT a.from_md5 FROM test_basic_diff_map a)";
			DataBaseHandler.getInstance().executeDml(query);
			
			query = "INSERT INTO test_basic_diff_map (map_type,from_md5,to_md5) "+
					"SELECT 'mapAddAss','',b.values_md5 "+
					"FROM test_evol_versions b WHERE b.version='new' AND b.base_type='association' AND b.values_md5 NOT IN (SELECT a.from_md5 FROM test_basic_diff_map a)";
			DataBaseHandler.getInstance().executeDml(query);
			
			query = "INSERT INTO test_basic_diff_map (map_type,from_md5,to_md5) "+
					"SELECT 'mapDelAss','',b.values_md5 "+
					"FROM test_evol_versions b WHERE b.version='old' AND b.base_type='association' AND b.values_md5 NOT IN (SELECT a.from_md5 FROM test_basic_diff_map a)";
			DataBaseHandler.getInstance().executeDml(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
