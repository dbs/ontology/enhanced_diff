package org.gomma.diff.match;

import java.sql.SQLException;
import java.util.Properties;

import org.gomma.diff.utils.DataBaseHandler;


public class AttAttMatcher {
	public int matchMethod;
	public Properties props;
	
	public AttAttMatcher(int matchMethod,Properties props) {
		this.matchMethod = matchMethod;
		this.props = props;
	}
	
	public void execute() {
		switch (this.matchMethod) {
		case MatchGlobals.FULL_EXACT_MATCH:
			this.doFullExactMatch();
			break;
		
		case MatchGlobals.TYPE_EXACT_MATCH:
			this.doTypeExactMatch();
			break;
			
		default:
			break;
		}
	}
	
	private void doFullExactMatch() {
		String att_name = props.getProperty("att_name");
		String query = "INSERT into test_basic_diff_map (map_type,from_md5,to_md5) SELECT 'mapAttAtt',a.values_md5,b.values_md5 ";
		query += "FROM test_evol_versions a, test_evol_versions b WHERE a.base_type='Attribute' AND b.base_type='Attribute' AND a.version='old' AND b.version='new' AND a.value2 = '"+att_name+"' AND b.value2 = '"+att_name+"' AND a.values_md5 = b.values_md5";
		try {
			DataBaseHandler.getInstance().executeDml(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void doTypeExactMatch() {
		String att_name = props.getProperty("att_name");
		String query = "INSERT into test_basic_diff_map (map_type,from_md5,to_md5) SELECT 'mapAttAtt',a.values_md5,b.values_md5 ";
		query += "FROM test_evol_versions a, test_evol_versions b WHERE a.base_type='Attribute' AND b.base_type='Attribute' AND a.version='old' AND b.version='new' AND a.value2 = '"+att_name+"' AND b.value2 = '"+att_name+"' AND a.value1 = b.value1";
		try {
			DataBaseHandler.getInstance().executeDml(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
