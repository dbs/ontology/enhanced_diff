package org.gomma.diff;

import org.gomma.diff.utils.DataBaseHandler;



public class DiffTester_DMoz {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception  {
		long start = System.currentTimeMillis();

		//Jahresvergleich 2008-2009 DMoz-Football
		String ontology 		= "DMoz-Football";
		int lds 				= 1;
		String start_version	= "01-2008";	
		String start_version_db = "2008-01-01";	//2008-01-01//2009-01-01
		String end_version	  	= "01-2009";	
		String end_version_db 	= "2009-01-01";	//2009-01-01//2010-01-01
		System.out.println("Start querying for basic match ... ");
		//GOBasicMatch.computeBasicDiff(lds, start_version_db, end_version_db, "E:/workspace/enhancedDiff/DMoz-Evaluation/"+ontology+"/"+start_version+"_"+end_version+"/baseMatch_"+ontology+"_"+start_version+"_"+end_version+".txt");
		System.out.println("Done in "+(System.currentTimeMillis()-start)+" ms\n");
		
		doGOExample(ontology,start_version,end_version);		
		System.out.println("-------\nOverall elapsed time: "+(System.currentTimeMillis()-start)+" ms");		
	}
	
	public static void doGOExample(String ontology,String start_version,String end_version) throws Exception {
		DataBaseHandler.getInstance().createDatabaseConnection(Globals.DB_URL,Globals.DB_USER,Globals.DB_PW);
		DiffExecutor.getSingleton().integrateVersionData("E:/workspace/enhancedDiff/DMoz-Evaluation/"+ontology+"/"+start_version+"_"+end_version+"/integration_"+ontology+"_"+start_version+"_"+end_version+".sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("E:/workspace/enhancedDiff/DMoz-Evaluation/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("E:/workspace/enhancedDiff/DMoz-Evaluation/Rule_DMoz.xml");
		DiffExecutor.getSingleton().applyRules();
		DiffExecutor.getSingleton().mergeResultActions();
		DiffExecutor.getSingleton().retrieveAndStoreHighLevelActions();
		
	}	
}
