package org.gomma.diff.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.gomma.diff.Globals;



public class DataBaseHandler {

	private static DataBaseHandler singleton = null;
	private Connection databaseConnection = null;
	
	public static DataBaseHandler getInstance() {
		if (singleton == null) {
			synchronized(DataBaseHandler.class) {
				if (singleton==null)
					singleton = new DataBaseHandler();
			}
		}
		return singleton;
	}

	private DataBaseHandler() {}
	
	public Connection getDatabaseConnection() throws SQLException {
		if (databaseConnection==null) {
			try {
				this.createDatabaseConnection();
				return this.databaseConnection;
			} catch (Exception e) {
				return null;
			}
		} else {
			if (!this.databaseConnection.isValid(3000)) {
				try {
					this.createDatabaseConnection();
					return this.databaseConnection;
				} catch (Exception e) {
					return null;
				}
			}
			return this.databaseConnection;
		}
	}
	

	public void createDatabaseConnection(String url, String user, String pw)
	throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		this.databaseConnection  = DriverManager.getConnection(url, user, pw);
	
	}
	
	public void createDatabaseConnection()
	throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		this.databaseConnection  = DriverManager.getConnection(Globals.DB_URL, Globals.DB_USER, Globals.DB_PW);
	
	}
	
	public void closeDatabaseConnection() {
	
	}
	
	public int executeDml(String dmlQuery) throws SQLException {
		
		Connection dbCon = null;
		Statement stmt = null;
		int affectedRows=0;
		
		dbCon = this.getDatabaseConnection();
		stmt = dbCon.createStatement();
		affectedRows = stmt.executeUpdate(dmlQuery);
		stmt.close(); 
		
		return(affectedRows);
	}
	
	public int[] executeDml(String[] dmlQueries) throws SQLException {
		
		Connection dbCon = null;
		Statement stmt = null;
		int[] affectedRows = new int[]{};
		
		dbCon = this.getDatabaseConnection();
		stmt = dbCon.createStatement();
		for (int i=0;i<dmlQueries.length;i++) 
			stmt.addBatch(dmlQueries[i]);
		affectedRows = stmt.executeBatch();
		stmt.close();
		
		return(affectedRows);
	}
	
	public ResultSet executeSelect(String sqlQuery) throws SQLException {
	
		Connection dbCon = null;
		Statement stmt = null;
		ResultSet rs = null;
		
		dbCon = this.getDatabaseConnection();
		stmt = dbCon.createStatement();
		rs = stmt.executeQuery(sqlQuery);
		
		return rs;
	}
	
	public void closeStatement(Statement stmt) throws SQLException {
		Connection dbCon = null; 
		
	
		dbCon = stmt.getConnection();
		
		stmt.close(); 
		dbCon.close();
	} 
	
	public void closeStatement(ResultSet rs) throws SQLException {
		Connection dbCon = null;
		Statement stmt = null;
		
		stmt = rs.getStatement();
		dbCon = stmt.getConnection();
		rs.close();
		if (stmt!=null) stmt.close(); 
		if (dbCon!=null && !dbCon.isClosed()) dbCon.close();
	}
	
	public PreparedStatement prepareStatement(String sqlQuery) throws SQLException {
		Connection dbCon = null;
		PreparedStatement pStmt = null;
		
		dbCon = this.getDatabaseConnection();
		pStmt = dbCon.prepareStatement(sqlQuery);
		
		return pStmt;
	}
}
