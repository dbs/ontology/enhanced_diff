package org.gomma.diff.utils;

import java.io.RandomAccessFile;
import java.sql.ResultSet;

public class GOBasicMatch {

	public static void main(String[] args) throws Exception {
		computeBasicDiff(1, "2008-01-15", "2008-12-15", "E:/Daten_Hartung/diffTest.txt");
	}
	
	public static void computeBasicDiff(int ldsID, String fromDate, String toDate, String fileLocation) throws Exception {
		RandomAccessFile file = new RandomAccessFile(fileLocation,"rw");
		
		DataBaseHandler.getInstance().createDatabaseConnection("jdbc:mysql://aprilia.izbi.uni-leipzig.de:3306/gomma_new_rep?autoReconnect=true", "mysql", "mysql");
		
		String exactObjMatch = "select 'mapObj',o1.accession,o2.accession "+
				"from gomma_obj_versions o1,gomma_obj_versions o2 "+
				"where o1.lds_id_fk = "+ldsID+"  and '"+fromDate+"' between o1.date_from and o1.date_to "+
				"AND o2.lds_id_fk = "+ldsID+" and '"+toDate+"' between o2.date_from and o2.date_to "+
				"AND o1.accession = o2.accession";
		writeResultToFile(3, DataBaseHandler.getInstance().executeSelect(exactObjMatch), file);
		
		String synonymMatch = "select 'mapObj',o1.accession,o2.accession "+
				"from gomma_obj_versions o1,gomma_obj_versions o2, gomma_attributes a2, gomma_obj_att_values av2 "+
				"where o1.lds_id_fk = "+ldsID+" and '"+fromDate+"' between o1.date_from and o1.date_to "+
				"AND o2.lds_id_fk = "+ldsID+" and '"+toDate+"' between o2.date_from and o2.date_to "+
				"AND a2.att_id = av2.att_id_fk AND av2.obj_id_fk = o2.obj_id AND a2.att_name = 'synonym' " +
				"AND '"+toDate+"' between av2.date_from and av2.date_to "+
				"AND o1.accession = av2.att_value_s";
		writeResultToFile(3, DataBaseHandler.getInstance().executeSelect(synonymMatch), file);
		
		String addObjects = "Select 'addObj',o3.accession "+
				"from gomma_obj_versions o3 "+
				"where o3.lds_id_fk = "+ldsID+" and '"+toDate+"' between o3.date_from and o3.date_to "+
				"and o3.accession NOT IN "+
				"( "+
				"select o2.accession "+
				"from gomma_obj_versions o1,gomma_obj_versions o2  "+
				"where o1.lds_id_fk = "+ldsID+" and '"+fromDate+"' between o1.date_from and o1.date_to "+
				"AND o2.lds_id_fk = "+ldsID+" and '"+toDate+"' between o2.date_from and o2.date_to "+
				"AND o1.accession = o2.accession "+
				"UNION "+
				"select o2.accession "+
				"from gomma_obj_versions o1,gomma_obj_versions o2, gomma_attributes a2, gomma_obj_att_values av2 "+
				"where o1.lds_id_fk = "+ldsID+" and '"+fromDate+"' between o1.date_from and o1.date_to "+
				"AND o2.lds_id_fk = "+ldsID+" and '"+toDate+"' between o2.date_from and o2.date_to AND a2.att_id = av2.att_id_fk " +
				"AND av2.obj_id_fk = o2.obj_id AND a2.att_name = 'synonym' AND '"+toDate+"' between av2.date_from and av2.date_to "+
				"AND o1.accession = av2.att_value_s)";
		writeResultToFile(2, DataBaseHandler.getInstance().executeSelect(addObjects), file);
		
		String deletedObjects = "Select 'delObj',o3.accession "+
				"from gomma_obj_versions o3 "+
				"where o3.lds_id_fk ="+ldsID+" and '"+fromDate+"' between o3.date_from and o3.date_to "+
				"and o3.accession NOT IN "+
				"( "+
				"select o1.accession "+
				"from gomma_obj_versions o1,gomma_obj_versions o2 "+
				"where o1.lds_id_fk = "+ldsID+" and '"+fromDate+"' between o1.date_from and o1.date_to "+
				"AND o2.lds_id_fk = "+ldsID+" and '"+toDate+"' between o2.date_from and o2.date_to "+
				"AND o1.accession = o2.accession "+
				"UNION "+
				"select o1.accession "+
				"from gomma_obj_versions o1,gomma_obj_versions o2, gomma_attributes a2, gomma_obj_att_values av2 "+
				"where o1.lds_id_fk = "+ldsID+" and '"+fromDate+"' between o1.date_from and o1.date_to "+
				"AND o2.lds_id_fk = "+ldsID+" and '"+toDate+"' between o2.date_from and o2.date_to AND a2.att_id = av2.att_id_fk AND av2.obj_id_fk = o2.obj_id AND a2.att_name = 'synonym' AND '"+toDate+"' between av2.date_from and av2.date_to "+
				"AND o1.accession = av2.att_value_s)";
		writeResultToFile(2, DataBaseHandler.getInstance().executeSelect(deletedObjects), file);

		System.out.println("Object mapping in file ...");
		
		String simpleAttributeMapping = "select 'mapAtt',o1.accession,a1.att_name,av1.att_value_s,o2.accession,a2.att_name,av2.att_value_s "+
				"from gomma_attributes a1, gomma_obj_att_values av1, gomma_obj_versions o1, gomma_attributes a2, gomma_obj_att_values av2, gomma_obj_versions o2 "+
				"where o1.lds_id_fk = "+ldsID+" and '"+fromDate+"' between av1.date_from and av1.date_to and o1.obj_id = av1.obj_id_fk and av1.att_id_fk = a1.att_id and a1.att_name IN ('name','definition','isObsolete') "+
				"and o2.lds_id_fk = "+ldsID+" and '"+toDate+"' between av2.date_from and av2.date_to and o2.obj_id = av2.obj_id_fk and av2.att_id_fk = a2.att_id and a2.att_name IN ('name','definition','isObsolete') "+
				"and o1.obj_id = o2.obj_id and a1.att_id = a2.att_id";
		writeResultToFile(7, DataBaseHandler.getInstance().executeSelect(simpleAttributeMapping), file);

		String simpleAttributeAdditions = "Select 'addAtt',o3.accession,a3.att_name,av3.att_value_s "+
				"from gomma_attributes a3, gomma_obj_att_values av3, gomma_obj_versions o3 "+
				"where o3.lds_id_fk = "+ldsID+" and '"+toDate+"' between av3.date_from and av3.date_to and o3.obj_id = av3.obj_id_fk and av3.att_id_fk = a3.att_id and a3.att_name IN ('name','definition','isObsolete') "+
				"and (o3.accession,a3.att_name,av3.att_value_s) "+
				"NOT IN "+
				"( "+
				"select o2.accession,a2.att_name,av2.att_value_s "+
				"from gomma_attributes a1, gomma_obj_att_values av1, gomma_obj_versions o1, gomma_attributes a2, gomma_obj_att_values av2, gomma_obj_versions o2 "+
				"where o1.lds_id_fk = "+ldsID+" and '"+fromDate+"' between av1.date_from and av1.date_to and o1.obj_id = av1.obj_id_fk and av1.att_id_fk = a1.att_id and a1.att_name IN ('name','definition','isObsolete') "+
				"and o2.lds_id_fk = "+ldsID+" and '"+toDate+"' between av2.date_from and av2.date_to and o2.obj_id = av2.obj_id_fk and av2.att_id_fk = a2.att_id and a2.att_name IN ('name','definition','isObsolete') "+
				"and o1.obj_id = o2.obj_id and a1.att_id = a2.att_id)";
		writeResultToFile(4, DataBaseHandler.getInstance().executeSelect(simpleAttributeAdditions), file);

		String simpleAttributeDeletions = "Select 'delAtt',o3.accession,a3.att_name,av3.att_value_s "+
				"from gomma_attributes a3, gomma_obj_att_values av3, gomma_obj_versions o3 "+
				"where o3.lds_id_fk = "+ldsID+" and '"+fromDate+"' between av3.date_from and av3.date_to and o3.obj_id = av3.obj_id_fk and av3.att_id_fk = a3.att_id and a3.att_name IN ('name','definition','isObsolete') "+
				"and (o3.accession,a3.att_name,av3.att_value_s) "+
				"NOT IN "+
				"( "+
				"select o1.accession,a1.att_name,av1.att_value_s "+
				"from gomma_attributes a1, gomma_obj_att_values av1, gomma_obj_versions o1, gomma_attributes a2, gomma_obj_att_values av2, gomma_obj_versions o2 "+
				"where o1.lds_id_fk = "+ldsID+" and '"+fromDate+"' between av1.date_from and av1.date_to and o1.obj_id = av1.obj_id_fk and av1.att_id_fk = a1.att_id and a1.att_name IN ('name','definition','isObsolete') "+
				"and o2.lds_id_fk = "+ldsID+" and '"+toDate+"' between av2.date_from and av2.date_to and o2.obj_id = av2.obj_id_fk and av2.att_id_fk = a2.att_id and a2.att_name IN ('name','definition','isObsolete') "+
				"and o1.obj_id = o2.obj_id and a1.att_id = a2.att_id)";
		writeResultToFile(4, DataBaseHandler.getInstance().executeSelect(simpleAttributeDeletions), file);
		
		String attributeSynonymExact = "select 'mapAtt',o1.accession,a1.att_name,av1.att_value_s,o2.accession,a2.att_name,av2.att_value_s "+
				"from gomma_attributes a1, gomma_obj_att_values av1, gomma_obj_versions o1, gomma_attributes a2, gomma_obj_att_values av2, gomma_obj_versions o2 "+
				"where o1.lds_id_fk = "+ldsID+" and '"+fromDate+"' between av1.date_from and av1.date_to and o1.obj_id = av1.obj_id_fk and av1.att_id_fk = a1.att_id and a1.att_name = 'synonym' "+
				"and o2.lds_id_fk = "+ldsID+" and '"+toDate+"' between av2.date_from and av2.date_to and o2.obj_id = av2.obj_id_fk and av2.att_id_fk = a2.att_id and a2.att_name = 'synonym' "+
				"and o1.obj_id = o2.obj_id and a1.att_id = a2.att_id and av1.att_value_s = av2.att_value_s";
		writeResultToFile(7, DataBaseHandler.getInstance().executeSelect(attributeSynonymExact), file);
		
		String attributeSynonymChange = "select 'addAtt',o.accession, a.att_name, av.att_value_s "+
				"from gomma_attributes a, gomma_obj_att_values av, gomma_obj_versions o "+
				"where o.lds_id_fk = "+ldsID+" and '"+toDate+"' between av.date_from and av.date_to and NOT ('"+fromDate+"' between av.date_from and av.date_to) "+
				"and o.obj_id = av.obj_id_fk and av.att_id_fk = a.att_id and a.att_name = 'synonym' "+
				"UNION "+
				"select 'delAtt',o.accession, a.att_name, av.att_value_s "+
				"from gomma_attributes a, gomma_obj_att_values av, gomma_obj_versions o "+
				"where o.lds_id_fk = "+ldsID+" and '"+fromDate+"' between av.date_from and av.date_to and NOT ('"+toDate+"' between av.date_from and av.date_to) "+
				"and o.obj_id = av.obj_id_fk and av.att_id_fk = a.att_id and a.att_name = 'synonym'";
		writeResultToFile(4, DataBaseHandler.getInstance().executeSelect(attributeSynonymChange), file);
		
		System.out.println("Attribute mapping in file ...");
		
		String relationshipExact = "select 'mapAss',src1.accession,r1.name,tar1.accession,src2.accession,r2.name,tar2.accession "+
				"from gomma_lds_structure s1, gomma_obj_versions src1, gomma_obj_versions tar1, gomma_rela_types r1, gomma_lds_structure s2, gomma_obj_versions src2, gomma_obj_versions tar2,gomma_rela_types r2 "+
				"where src1.lds_id_fk = "+ldsID+" and tar1.lds_id_fk = "+ldsID+" and '"+fromDate+"' between s1.date_from and s1.date_to "+
				"and s1.p_obj_id_fk = tar1.obj_id and s1.c_obj_id_fk = src1.obj_id and r1.rela_type_id = s1.rela_type_id_fk "+
				"and src2.lds_id_fk = "+ldsID+" and tar2.lds_id_fk = "+ldsID+" and '"+toDate+"' between s2.date_from and s2.date_to "+
				"and s2.p_obj_id_fk = tar2.obj_id and s2.c_obj_id_fk = src2.obj_id and r2.rela_type_id = s2.rela_type_id_fk "+
				"and src1.accession = src2.accession and r1.name = r2.name and tar1.accession = tar2.accession";
		writeResultToFile(7, DataBaseHandler.getInstance().executeSelect(relationshipExact), file);
		
		String relationshipChanges = "select 'addAss',src.accession,r.name,tar.accession "+
				"from gomma_lds_structure s, gomma_obj_versions src, gomma_obj_versions tar, gomma_rela_types r "+
				"where src.lds_id_fk = "+ldsID+" and tar.lds_id_fk = "+ldsID+" "+
				"and '"+toDate+"' between s.date_from and s.date_to and NOT ('"+fromDate+"' between s.date_from and s.date_to) "+
				"and s.p_obj_id_fk = tar.obj_id and s.c_obj_id_fk = src.obj_id "+
				"and r.rela_type_id = s.rela_type_id_fk "+
				"UNION "+
				"select 'delAss',src.accession,r.name,tar.accession "+
				"from gomma_lds_structure s, gomma_obj_versions src, gomma_obj_versions tar, gomma_rela_types r "+
				"where src.lds_id_fk = "+ldsID+" and tar.lds_id_fk = "+ldsID+" "+
				"and '"+fromDate+"' between s.date_from and s.date_to and NOT ('"+toDate+"' between s.date_from and s.date_to) "+
				"and s.p_obj_id_fk = tar.obj_id and s.c_obj_id_fk = src.obj_id "+
				"and r.rela_type_id = s.rela_type_id_fk;";
		writeResultToFile(4, DataBaseHandler.getInstance().executeSelect(relationshipChanges), file);
		
		System.out.println("Relationship mapping in file ...");
		
		file.close();
	}
	
	public static void writeResultToFile(int numberOfCols, ResultSet rs, RandomAccessFile file) throws Exception {
		while (rs.next()) {
			String line = rs.getString(1);
			for (int i=1;i<numberOfCols;i++) {
				line += "\t"+rs.getString(i+1);
			}
			file.writeBytes(line+"\n");
		}
		rs.close();
	}
}
