package org.gomma.diff;

import org.gomma.diff.utils.*;

public class DiffTester_GOAll {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception  {
		long start = System.currentTimeMillis();
		String ontology 		= "GO-all";
		String start_version	= "01-2008";	//01-2008//01-2009
		String end_version	  	= "01-2009";	//01-2009//01-2010	
		
		doGOExample(ontology,start_version,end_version);
		System.out.println("Done in "+(System.currentTimeMillis()-start)+" ms\n");
	}
	
	
	public static void doGOExample(String ontology,String start_version,String end_version) throws Exception {
		DataBaseHandler.getInstance().createDatabaseConnection(Globals.DB_URL,Globals.DB_USER,Globals.DB_PW);
		//DiffExecutor.getSingleton().setupRepository();
		//DiffExecutor.getSingleton().integrateVersionData("E:/Daten_Hartung/workspace3.6/Enhanced Diff/GeneOntology-Evaluation/"+ontology+"/"+start_version+"_"+end_version+"/integration_"+ontology+"_"+start_version+"_"+end_version+".sql");
		DiffExecutor.getSingleton().loadChangeActionDesc("E:/Daten_Hartung/workspace3.6/Enhanced Diff/GeneOntology-Evaluation/ChangeActions.xml");
		DiffExecutor.getSingleton().loadRules("E:/Daten_Hartung/workspace3.6/Enhanced Diff/GeneOntology-Evaluation/Rule_GO.xml");
		DiffExecutor.getSingleton().applyRules();
		DiffExecutor.getSingleton().mergeResultActions();
		DiffExecutor.getSingleton().retrieveAndStoreHighLevelActions();
		
	}	
}
