package org.gomma.annotationMigrationAnalysis;

import java.sql.SQLException;

import org.gomma.api.util.DataSourceSystems;
import org.gomma.api.util.DatabaseConnectionData;
import org.gomma.api.util.DatabaseHandler;
import org.gomma.util.SystemPropertyPool;

public class DBConnection {

	public void open(String database) throws Exception{
		new SystemPropertyPool.Builder().dataSourceSystem(DataSourceSystems.MYSQL).build();
		DatabaseConnectionData connectionData;

		String url  = "jdbc:mysql://aprilia.izbi.uni-leipzig.de:3306/"+database;
		String user = "mysql";
		String pw = "mysql";
		String driver = "com.mysql.jdbc.Driver";
		connectionData= new DatabaseConnectionData(driver, url, user, pw);
		DatabaseHandler.getInstance().setDatabaseConnectionData(connectionData);
		DatabaseHandler.getInstance().createDatabaseConnection();
		
	}
	
}
