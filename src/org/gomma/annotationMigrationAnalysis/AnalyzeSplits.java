package org.gomma.annotationMigrationAnalysis;

import java.io.File;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.sql.ResultSet;
import java.util.HashMap;

import org.gomma.api.util.DatabaseHandler;

public class AnalyzeSplits {

	public static void main(String[] args) throws Exception {

		DBConnection db1 = new DBConnection();
		db1.open("gomma_life_science");
		String query1 = "select distinct accession, att_value_s " +
				"from gomma_obj_versions, gomma_obj_att_values " +
				"where obj_id=obj_id_fk " +
				"and att_id_fk=1";

		System.out.println(query1);
		ResultSet rs2 = DatabaseHandler.getInstance().executeSelect(query1);
		
		HashMap<String,String> goAccessionNames = new HashMap<String,String>();
		
		while(rs2.next()){
			goAccessionNames.put(rs2.getString(1),rs2.getString(2));
		}
		System.out.println(goAccessionNames.size());
		rs2.close();
	
		
		DBConnection db2 = new DBConnection();
		db2.open("GOA");
		
		String query2 = "SELECT distinct db_object_id, db_object_name " +
						"FROM GOA_full_versions " +
						"WHERE version >= 63;";
		System.out.println(query2);
		ResultSet rs1 = DatabaseHandler.getInstance().executeSelect(query2);
		
		HashMap<String,String> proteinNames = new HashMap<String,String>();
		
		while(rs1.next()){
			proteinNames.put(rs1.getString(1),rs1.getString(2));
		}
		System.out.println(proteinNames.size());
		rs1.close();
		
		//get splits
		String fileLocation = "E:/workspace/enhancedDiff/AnnotationMigrationAnalysis/splits.txt";
		
		RandomAccessFile file = new RandomAccessFile(fileLocation, "rw");
		String line;
		String writeToFile1="";
		String writeToFile2="Protein\told GO Annotation (affected by split) \tnew \"refined\" GO Annotation\n";
		
		while ((line=file.readLine())!=null) {
			String[] lineArray			= line.split("\\] influenced by split\\(");
			String[] proteins			= lineArray[0].replace("[", "").split(", ");
			String[] goAccessions		= lineArray[1].split(",\\[");
			String   splittedAccession	= goAccessions[0];
			String[] splittedInto		= goAccessions[1].replace("])", "").split(", ");
			
			writeToFile1+="Betroffene Proteine:\n";
			for(String p:proteins){
				//System.out.println(p+"\t"+proteinNames.get(p));
				writeToFile1+=p+"\t"+proteinNames.get(p)+"\n";
			}
			//System.out.println("\n");
			//System.out.println(splittedAccession + " splitted into:\n");
			writeToFile1+="\n"+splittedAccession + " splitted into:\n\n";
			for(String s:splittedInto){
				//System.out.println(s+"\t"+goAccessionNames.get(s));
				writeToFile1+=s+"\t"+goAccessionNames.get(s)+"\n";
			}
			
			for(String p:proteins){
				/*String query3 = "SELECT * " +
								"FROM GOA_full_versions " +
								"WHERE version = 94 " +
								"and db_object_id = \""+p+"\" " +
								"and go_id = \""+splittedAccession+"\"";
				//System.out.println(query3);
				ResultSet rs3 = DatabaseHandler.getInstance().executeSelect(query3);
				if(!rs3.next()){					
					System.out.println(p + "\t"+splittedAccession);
					*/
					String goString = "\""+splittedInto[0]+"\"";
					for (int i = 1; i < splittedInto.length; i++) {
						goString += ", \""+splittedInto[i]+"\"";
					}
					
					String query4 = "SELECT DISTINCT db_object_id,go_id " +
									"FROM GOA_full_versions " +
									"WHERE version = 94 " +
									"and db_object_id = \""+p+"\" " +
									"and go_id IN ("+goString+")";
					//System.out.println(query4);
					ResultSet rs4 = DatabaseHandler.getInstance().executeSelect(query4);
					
					while(rs4.next()){
						if (!splittedAccession.equals(rs4.getString(2))) {
							writeToFile2+= rs4.getString(1)+" ("+proteinNames.get(rs4.getString(1))+")"
							+"\t"+splittedAccession+" ("+goAccessionNames.get(splittedAccession)+")"
							+"\t"+rs4.getString(2)+" ("+goAccessionNames.get(rs4.getString(2))+")\n";
						}
					}
					rs4.close();
				//}
				//rs3.close();
			}
			System.out.println("#####");
			//writeToFile1+="#####\n";		
		}
		//save results
		String locationForResult1 = "E:/workspace/enhancedDiff/AnnotationMigrationAnalysis/SplitsWithAccessionNames.txt";		
		File resultFile1 = new File(locationForResult1);
		PrintWriter writer1 = new PrintWriter(resultFile1);				
		writer1.write(writeToFile1);		
		writer1.flush();
		writer1.close();

		String locationForResult2 = "E:/workspace/enhancedDiff/AnnotationMigrationAnalysis/refinedAnnotations.txt";
		
		File resultFile2 = new File(locationForResult2);
		PrintWriter writer2 = new PrintWriter(resultFile2);				
		writer2.write(writeToFile2);		
		writer2.flush();
		writer2.close();

		
	}
}
