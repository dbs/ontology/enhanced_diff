package org.gomma.annotationMigrationAnalysis;

import java.io.File;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.gomma.api.util.DatabaseHandler;

public class AnalyzeObsoletes {

	public static void main(String[] args) throws Exception {

		DBConnection db1 = new DBConnection();
		db1.open("gomma_life_science");
		String query1 = "select distinct accession, att_value_s " +
				"from gomma_obj_versions, gomma_obj_att_values " +
				"where obj_id=obj_id_fk " +
				"and att_id_fk=1";

		System.out.println(query1);
		ResultSet rs2 = DatabaseHandler.getInstance().executeSelect(query1);
		
		HashMap<String,String> goAccessionNames = new HashMap<String,String>();
		
		while(rs2.next()){
			goAccessionNames.put(rs2.getString(1),rs2.getString(2));
		}
		System.out.println(goAccessionNames.size());
		rs2.close();
	
		
		DBConnection db2 = new DBConnection();
		db2.open("GOA");
		
		String query2 = "SELECT distinct db_object_id, db_object_name " +
						"FROM GOA_full_versions " +
						"WHERE version >= 63;";
		System.out.println(query2);
		ResultSet rs1 = DatabaseHandler.getInstance().executeSelect(query2);
		
		HashMap<String,String> proteinNames = new HashMap<String,String>();
		
		while(rs1.next()){
			proteinNames.put(rs1.getString(1),rs1.getString(2));
		}
		System.out.println(proteinNames.size());
		rs1.close();
		
		//get splits
		String fileLocation = "E:/workspace/enhancedDiff/AnnotationMigrationAnalysis/obsoletes.txt";
		
		RandomAccessFile file = new RandomAccessFile(fileLocation, "rw");
		String line;
		String writeToFile1="";
		String writeToFile2="Protein\tobsoleteAnnotation\tnew GO Annotation\n";
		
		while ((line=file.readLine())!=null) {
			String[] lineArray		= line.split("\\] influenced by toObsolete\\(");
			String[] proteins		= lineArray[0].replace("[", "").split(", ");
			String goAccession		= lineArray[1].replace(")", "");
			//[P56937] influenced by merge([GO:0000253, GO:0050576],GO:0000253)
			
			for(String p:proteins){
				writeToFile1+=p+"\t"+proteinNames.get(p)+"\n";
			}
			
			for(String p:proteins){
				String query3 = "SELECT DISTINCT db_object_id,go_id " +
								"FROM GOA_full_versions " +
								"WHERE version = 63 " +
								"and db_object_id = \""+p+"\";";
			
				ResultSet rs3 = DatabaseHandler.getInstance().executeSelect(query3);
				String oldAnnotation = "";
				List<String> oldAnnotations = new Vector<String>();
				while(rs3.next()){					
					oldAnnotation += ", "+rs3.getString(2);
					oldAnnotations.add(rs3.getString(2));
				}
				if(oldAnnotation.length()>1){
					oldAnnotation=oldAnnotation.substring(1);
				}
				
				rs3.close();
			
				String query4 = "SELECT DISTINCT db_object_id,go_id " +
								"FROM GOA_full_versions " +
								"WHERE version = 94 " +
								"and db_object_id = \""+p+"\";";
				
				//System.out.println(query4);
				ResultSet rs4 = DatabaseHandler.getInstance().executeSelect(query4);
				String newAnnotation = "";
				List<String> newAnnotations = new Vector<String>();
				while(rs4.next()){
					newAnnotation += ", "+rs4.getString(2)+" ("+goAccessionNames.get(rs4.getString(2))+")";
					newAnnotations.add(rs4.getString(2));
				}
				if(newAnnotation.length()>1){
					newAnnotation=newAnnotation.substring(1);
				}
				System.out.println(newAnnotations.size());
				for(String oldA:oldAnnotations){
					if(newAnnotations.contains(oldA)){
						newAnnotations.remove(oldA);
						//System.out.println("remove "+oldA);
					}
				}
				System.out.println(newAnnotations.size());
				writeToFile2+= p+" ("+proteinNames.get(p)+")" 
						//+"\t"+oldAnnotation 
						+"\t"+goAccession +" ("+goAccessionNames.get(goAccession)+")"
						+"\t"+newAnnotation+"\n";
				System.out.println(p+" ("+proteinNames.get(p)+")" 
						//+"\t"+oldAnnotation 
						+"\ttoObsolete("+goAccession +")"
						+"\t=\t"+goAccessionNames.get(goAccession)+")"
						+"\t"+newAnnotation+"\n");
				rs4.close();
			}			
		}
		//save results
		String locationForResult1 = "E:/workspace/enhancedDiff/AnnotationMigrationAnalysis/ObsoletesWithAccessionNames.txt";		
		File resultFile1 = new File(locationForResult1);
		PrintWriter writer1 = new PrintWriter(resultFile1);				
		writer1.write(writeToFile1);		
		writer1.flush();
		writer1.close();

		String locationForResult2 = "E:/workspace/enhancedDiff/AnnotationMigrationAnalysis/newObsoletesAnnotations.txt";
		
		File resultFile2 = new File(locationForResult2);
		PrintWriter writer2 = new PrintWriter(resultFile2);				
		writer2.write(writeToFile2);		
		writer2.flush();
		writer2.close();

		
	}
}
