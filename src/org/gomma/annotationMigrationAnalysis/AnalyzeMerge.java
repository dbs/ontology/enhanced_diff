package org.gomma.annotationMigrationAnalysis;

import java.io.File;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.gomma.api.util.DatabaseHandler;

public class AnalyzeMerge {

	public static void main(String[] args) throws Exception {

		DBConnection db1 = new DBConnection();
		db1.open("gomma_life_science");
		String query1 = "select distinct accession, att_value_s " +
				"from gomma_obj_versions, gomma_obj_att_values " +
				"where obj_id=obj_id_fk " +
				"and att_id_fk=1";

		System.out.println(query1);
		ResultSet rs2 = DatabaseHandler.getInstance().executeSelect(query1);
		
		HashMap<String,String> goAccessionNames = new HashMap<String,String>();
		
		while(rs2.next()){
			goAccessionNames.put(rs2.getString(1),rs2.getString(2));
		}
		System.out.println(goAccessionNames.size());
		rs2.close();
	
		
		DBConnection db2 = new DBConnection();
		db2.open("GOA");
		
		String query2 = "SELECT distinct db_object_id, db_object_name " +
						"FROM GOA_full_versions " +
						"WHERE version >= 63;";
		System.out.println(query2);
		ResultSet rs1 = DatabaseHandler.getInstance().executeSelect(query2);
		
		HashMap<String,String> proteinNames = new HashMap<String,String>();
		
		while(rs1.next()){
			proteinNames.put(rs1.getString(1),rs1.getString(2));
		}
		System.out.println(proteinNames.size());
		rs1.close();
		
		//get splits
		String fileLocation = "E:/workspace/enhancedDiff/AnnotationMigrationAnalysis/merges.txt";
		
		RandomAccessFile file = new RandomAccessFile(fileLocation, "rw");
		String line;
		String writeToFile1="";
		String writeToFile2="Protein\told GO Annotation\taffected by merge\tnew \"refined\" GO Annotation\n";
		
		while ((line=file.readLine())!=null) {
			String[] lineArray			= line.split("\\] influenced by merge\\(\\[");
			String[] proteins			= lineArray[0].replace("[", "").split(", ");
			String[] goAccessions		= lineArray[1].split("\\],");
			String[] mergedAccessions	= goAccessions[0].split(", ");
			String	 mergedTo			= goAccessions[1].replace(")", "");
			
			//[P56937] influenced by merge([GO:0000253, GO:0050576],GO:0000253)
			
			writeToFile1+="Betroffene Proteine:\n";
			for(String p:proteins){
				//System.out.println(p+"\t"+proteinNames.get(p));
				writeToFile1+=p+"\t"+proteinNames.get(p)+"\n";
			}
			//System.out.println("\n");
			//System.out.println(splittedAccession + " splitted into:\n");
			writeToFile1+="\n";
			String mergedAccessionString="";
			for(String m:mergedAccessions){
				//System.out.println(s+"\t"+goAccessionNames.get(s));
				writeToFile1+=m+"\t"+goAccessionNames.get(m)+"\n";
				mergedAccessionString+=", \""+m+"\"";
			}
			mergedAccessionString=mergedAccessionString.substring(1);
			writeToFile1+="merged to:"+mergedTo+"\t"+goAccessionNames.get(mergedTo) + "\n\n";
			
			for(String p:proteins){
				/*String query3 = "SELECT * " +
								"FROM GOA_full_versions " +
								"WHERE version = 94 " +
								"and db_object_id = \""+p+"\" " +
								"and go_id = \""+splittedAccession+"\"";
				//System.out.println(query3);
				ResultSet rs3 = DatabaseHandler.getInstance().executeSelect(query3);
				if(!rs3.next()){					
					System.out.println(p + "\t"+splittedAccession);
					*/
					String query3 = "SELECT DISTINCT db_object_id,go_id " +
									"FROM GOA_full_versions " +
									"WHERE version = 63 " +
									"and db_object_id = \""+p+"\" " +
									"and go_id IN ("+mergedAccessionString+")";
				
					ResultSet rs3 = DatabaseHandler.getInstance().executeSelect(query3);
					String oldAnnotation = "";
					while(rs3.next()){					
						oldAnnotation += ", "+rs3.getString(2);
					}
					oldAnnotation=oldAnnotation.substring(1);
					rs3.close();
				
					String query4 = "SELECT DISTINCT db_object_id,go_id " +
									"FROM GOA_full_versions " +
									"WHERE version = 94 " +
									"and db_object_id = \""+p+"\" " +
									"and go_id = \""+mergedTo+"\"";
					//System.out.println(query4);
					ResultSet rs4 = DatabaseHandler.getInstance().executeSelect(query4);
					List<String> mergedAccessionsList = Arrays.asList(mergedAccessions); 
					while(rs4.next()){
						//if (!mergedAccessionsList.contains(rs4.getString(2))) {
							writeToFile2+= rs4.getString(1)+" ("+proteinNames.get(rs4.getString(1))+")" 
							+"\t"+oldAnnotation 
							+"\tmerge"+mergedAccessionsList+","+mergedTo//+" ("+goAccessionNames.get(splittedAccession)+")"
							+"\t"+rs4.getString(2)+" ("+goAccessionNames.get(rs4.getString(2))+")\n";
						
							
							
							System.out.println(rs4.getString(1)+" ("+proteinNames.get(rs4.getString(1))+") was annotated with" 
									+"\t"+oldAnnotation +" which was affected by"
									+"\t"+mergedAccessionsList//+" ("+goAccessionNames.get(splittedAccession)+")"
									+"\t"+rs4.getString(2)+" ("+goAccessionNames.get(rs4.getString(2))+")\n");
						//}
					}
					rs4.close();
				//}
				//rs3.close();
			}
			//System.out.println("#####");
			//writeToFile1+="#####\n";		
		}
		//save results
		String locationForResult1 = "E:/workspace/enhancedDiff/AnnotationMigrationAnalysis/MergesWithAccessionNames.txt";		
		File resultFile1 = new File(locationForResult1);
		PrintWriter writer1 = new PrintWriter(resultFile1);				
		writer1.write(writeToFile1);		
		writer1.flush();
		writer1.close();

		String locationForResult2 = "E:/workspace/enhancedDiff/AnnotationMigrationAnalysis/refinedMergeAnnotations.txt";
		
		File resultFile2 = new File(locationForResult2);
		PrintWriter writer2 = new PrintWriter(resultFile2);				
		writer2.write(writeToFile2);		
		writer2.flush();
		writer2.close();

		
	}
}
